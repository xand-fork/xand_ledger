//! This module contains transaction validation logic, as well as helpers for constructing
//! transactions

#![allow(non_snake_case)]

mod builder;
pub(crate) mod clear_redeem;
mod common;
mod create;
mod exiting_redeem;
mod redeem;
mod send;

pub use builder::*;
pub use clear_redeem::construct_clear_redeem_request_transaction;
pub use common::*;
pub use create::*;
pub use exiting_redeem::*;
pub use redeem::*;
pub use send::*;

use crate::*;
use alloc::vec::Vec;
use curve25519_dalek::scalar::Scalar;
use rand::{CryptoRng, RngCore};
use serde::{Deserialize, Serialize};

use crate::{crypto::get_G, verifiable_encryption::EncryptionVerificationError};
use sha2::{Digest, Sha256};

/// Given a one time key or identity tag check whether it matches the private key provided.
/// Returns true if the private key and public key matches, false otherwise
pub(crate) fn check_ownership<T: Into<CurveVector>>(
    into_vector: T,
    private_key: PrivateKey,
) -> bool {
    let vector = into_vector.into();
    private_key * vector.x == vector.y
}

/// Given a TransactionOutput, check whether it is owned by the private key provided.
pub fn check_txo_ownership(txo: TransactionOutput, private_key: PrivateKey) -> bool {
    check_ownership(txo.public_key(), private_key)
}

/// Returns whether the identity tag matches the private key
pub fn check_identity_tag_ownership(id_tag: IdentityTag, private_key: PrivateKey) -> bool {
    check_ownership(id_tag, private_key)
}

#[derive(Clone)]
pub struct Message(IdentityTag);

impl Message {
    pub fn new(id_tag: IdentityTag) -> Self {
        Message(id_tag)
    }

    pub fn get_bytes(&self) -> Vec<u8> {
        self.0.to_hash().to_vec()
    }
}

impl From<IdentityTag> for Message {
    fn from(tag: IdentityTag) -> Message {
        Message(tag)
    }
}

trait TxoCodec {
    fn generate_txo(output: SpendingOutput, private_key: PrivateKey) -> SendClaimsOutput;

    fn decode_txo(
        send_claims_output: SendClaimsOutput,
        private_key: PrivateKey,
    ) -> OpenedTransactionOutput;
}

/// Build a OneTimeKey for a given PublicKey. The OneTimeKey is derived using the message body, the
/// txo index, and the message sender's private key to form a random value that is only reproducible
/// for the sender but verifiable for the recipient.
pub fn derive_one_time_key(
    recipient_pubkey: PublicKey,
    message: Message,
    output_index: usize,
    issuer_private_key: PrivateKey,
) -> OneTimeKey {
    let r = txo_deterministic_randomness(message, output_index, issuer_private_key);
    let one_time_key = CurveVector {
        x: get_G() * r,
        y: (r * RistrettoPoint::from(recipient_pubkey)),
    };
    one_time_key.into()
}

/// Determine the PublicKey associated with a given OneTimeKey. The OneTimeKey is derived using the
/// message body, the txo index, and the message sender's private key to form a random value that is
/// only reproducible for the sender.
pub fn decode_one_time_key(
    key: OneTimeKey,
    message: Message,
    output_index: usize,
    issuer_private_key: PrivateKey,
) -> Option<PublicKey> {
    let r = txo_deterministic_randomness(message, output_index, issuer_private_key);
    let inv_r = r.invert();
    let pub_key = CurveVector {
        x: key.x() * inv_r,
        y: key.y() * inv_r,
    };
    if pub_key.x == get_G() {
        Some(pub_key.y.into())
    } else {
        None
    }
}

/// r_t in the yellow paper in 5.3.3.2
pub fn txo_deterministic_randomness(
    message: Message,
    output_index: usize,
    issuer_private_key: PrivateKey,
) -> Scalar {
    let mut hasher = Sha256::new();
    hasher.input(message.get_bytes());
    hasher.input(output_index.to_le_bytes());
    hasher.input(issuer_private_key.as_bytes());
    let mut hash = [0u8; 32];
    hash.copy_from_slice(hasher.result().as_slice());
    Scalar::from_bytes_mod_order(hash)
}

#[derive(Debug, PartialEq, Eq, Clone, Deserialize, Serialize)]
#[cfg_attr(feature = "std", derive(thiserror::Error))]
pub enum TransactionValidationError {
    #[cfg_attr(
        feature = "std",
        error("The bullet proof doesn't match the commitments (txos) of this transaction")
    )]
    RangeProofCommitmentMismatch,
    #[cfg_attr(feature = "std", error("Mismatch in opened commitment"))]
    OpenedCommitmentMismatch,
    #[cfg_attr(feature = "std", error("Cryptographically invalid bullet proof"))]
    InvalidBulletProof,
    #[cfg_attr(feature = "std", error("Invalida alpha"))]
    InvalidAlpha,
    #[cfg_attr(feature = "std", error("Invalid signature"))]
    InvalidSignature,
    #[cfg_attr(feature = "std", error("Invalid input set"))]
    InvalidInputSet,
    #[cfg_attr(feature = "std", error("Invalid encryption"))]
    InvalidEncryption(#[cfg_attr(feature = "std", source)] EncryptionVerificationError),
    #[cfg_attr(feature = "std", error("Constructed by banned member"))]
    ConstructedByBannedMember,
    #[cfg_attr(feature = "std", error("Banned member lists have different lengths"))]
    BannedMemberListDifferentLengths,
}

impl From<EncryptionVerificationError> for TransactionValidationError {
    fn from(e: EncryptionVerificationError) -> Self {
        TransactionValidationError::InvalidEncryption(e)
    }
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq)]
pub struct SpendingOutput {
    value: u64,
    key: PublicKey,
    encryption_key: EncryptionKey,
}

impl SpendingOutput {
    pub fn new(value: u64, key: PublicKey, encryption_key: EncryptionKey) -> SpendingOutput {
        SpendingOutput {
            value,
            key,
            encryption_key,
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PrivateInputs {
    pub spends: Vec<OpenedTransactionOutput>,
    pub outputs: Vec<SpendingOutput>,
    pub identity_input: IdentityTag,
}

impl PrivateInputs {
    pub fn new(
        spends: Vec<OpenedTransactionOutput>,
        outputs: Vec<SpendingOutput>,
        identity_input: IdentityTag,
    ) -> Self {
        PrivateInputs {
            spends,
            outputs,
            identity_input,
        }
    }

    pub fn spends(&self) -> &Vec<OpenedTransactionOutput> {
        &self.spends
    }

    pub fn identity_tag(&self) -> IdentityTag {
        self.identity_input
    }
}

#[derive(Clone, Debug, Deserialize, PartialEq, Eq, Serialize)]
pub struct PublicInputSet {
    pub txos: TransactionInputSet,
    pub identity_input: IdentityTag,
}

impl PublicInputSet {
    pub fn new(txos: TransactionInputSet, identity_input: IdentityTag) -> Self {
        PublicInputSet {
            txos,
            identity_input,
        }
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;
    use proptest::prelude::*;
    use zkplmt_test_helpers::FakeCsprng;

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(10))]
        #[test]
        fn test_check_public_key_ownership(rng_seed: [u8; 32]) {
            let mut csprng = FakeCsprng::from_seed(rng_seed);
            let private_key = Scalar::random(&mut csprng);
            let id_tag = IdentityTag::from_key_with_rand_base(private_key.into(), &mut csprng);
            assert!(check_ownership(
                OneTimeKey::from_id_with_randomness(id_tag, &mut csprng),
                private_key
            ));
            assert!(!check_ownership(
                OneTimeKey::from_id_with_randomness(id_tag, &mut csprng),
                Scalar::random(&mut csprng)
            ));
        }
    }

    prop_compose! {
        fn arb_u8_64()(
            bytes_1: [u8; 32],
            bytes_2: [u8; 32],
        ) -> [u8; 64] {
            let mut output = [0u8; 64];
            output[..32].copy_from_slice(&bytes_1);
            output[32..].copy_from_slice(&bytes_2);
            output
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(10))]
        #[test]
        fn can_roundtrip_one_time_key_from_public_key(
            message: [u8; 32],
            index: usize,
            issuer_private_key: [u8; 32],
            recipient_pub_key in arb_u8_64()
        ) {
            let issuer_private_key = Scalar::from_bytes_mod_order(issuer_private_key);
            let recipient = RistrettoPoint::from_uniform_bytes(&recipient_pub_key).into();
            let message_scalar = Scalar::from_bytes_mod_order(message);
            let identity_tag = IdentityTag::from_key_with_generator_base(message_scalar.into());
            let message = Message::new(identity_tag);

            let one_time_key = derive_one_time_key(recipient, message.clone(), index, issuer_private_key);
            let decoded_recipient_key =
                decode_one_time_key(one_time_key, message, index, issuer_private_key).unwrap();

            assert_eq!(decoded_recipient_key, recipient);
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(10))]
        #[test]
        fn cant_decode_one_time_key_with_different_private_key(
            message: [u8; 32],
            index: usize,
            issuer_private_key: [u8; 32],
            other_private_key: [u8; 32],
            recipient_pub_key in arb_u8_64()
        ) {
            let issuer_private_key = Scalar::from_bytes_mod_order(issuer_private_key);
            let other_private_key = Scalar::from_bytes_mod_order(other_private_key);
            let recipient = RistrettoPoint::from_uniform_bytes(&recipient_pub_key).into();
            let message_scalar = Scalar::from_bytes_mod_order(message);
            let identity_tag = IdentityTag::from_key_with_generator_base(message_scalar.into());
            let message = Message::new(identity_tag);

            let one_time_key = derive_one_time_key(recipient, message.clone(), index, issuer_private_key);
            let decoded_recipient_key =
                decode_one_time_key(one_time_key, message, index, other_private_key);

            assert!(decoded_recipient_key.is_none());
        }
    }
}
