use crate::{
    clear_txo::IUtxo, esig_payload::ESigPayload, ClearTransactionOutput,
    ClearTransactionOutputRepository,
};
use std::{cell::RefCell, collections::HashSet};

#[derive(Default)]
pub struct FakeClearTransactionDataRepository {
    transaction_outputs: RefCell<HashSet<ClearTransactionOutput>>,
}

impl FakeClearTransactionDataRepository {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn seeded(seed: &[ClearTransactionOutput]) -> Self {
        let records = seed.iter().cloned().collect();
        Self {
            transaction_outputs: RefCell::new(records),
        }
    }

    pub fn current_txos(&self) -> HashSet<ClearTransactionOutput> {
        self.transaction_outputs.borrow().clone()
    }

    pub fn get_ueta_payload_for_utxo(&self, utxo: ClearTransactionOutput) -> ESigPayload {
        self.current_txos()
            .get(&utxo)
            .cloned()
            .unwrap()
            .esig_payload()
            .to_owned()
    }
}

impl ClearTransactionOutputRepository for FakeClearTransactionDataRepository {
    type Utxo = ClearTransactionOutput;

    fn contains(&self, output: &Self::Utxo) -> bool {
        self.transaction_outputs.borrow().contains(output)
    }

    fn add(&self, output: Self::Utxo) {
        let _ = self.transaction_outputs.borrow_mut().insert(output);
    }

    fn remove(&self, output: Self::Utxo) -> bool {
        self.transaction_outputs.borrow_mut().remove(&output)
    }
}
