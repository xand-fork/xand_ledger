//! This crate defines our domain models and logic for the xand ledger. IE: This is the code
//! that determines how transactions are validated/processed, as well as the shape of those
//! transactions. It is free of major dependencies and should remain so to provide a clean
//! environment for concentrating on domain code.
//!
//! Practically speaking this code is consumed in other places for two purposes:
//! * Validating transactions (within the substrate runtime)
//! * Constructing transactions (everywhere else)
//!
//! The crate has an `std` feature that is enabled by default. Disabling the feature allows the
//! crate to compile in no-std mode. The substrate runtime requires this, and no-std support must
//! be maintained.

#![crate_type = "lib"]
#![cfg_attr(not(feature = "std"), no_std)]
// TODO: Remove when closer to finalizing
#![allow(dead_code, unused_variables, clippy::result_large_err)]
// TODO re-enable and add docs - see https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6416
// #![warn(missing_docs)]

// Needed to support no_std (for most other uses, deprecated since Rust 2018)
#[macro_use]
extern crate alloc;

mod chain_logic;
/// Zero Knowledge Implementation
pub mod crypto;
mod errors;
mod model;

#[cfg(test)]
mod test;
#[cfg(any(test, feature = "test-helpers"))]
pub mod test_helpers;
mod transactions;

pub use chain_logic::{
    CreateRequestProcessor, ExitingRedeemRequestProcessor, RedeemRequestProcessor, RewardProcessor,
    SendClaimsProcessor,
};
pub use curve25519_dalek;
pub use errors::FailureReason;
pub use model::*;
pub use transactions::{
    check_identity_tag_ownership, check_txo_ownership, construct_clear_redeem_request_transaction,
    construct_create_request_transaction, construct_exiting_redeem_request_transaction,
    construct_redeem_request_transaction, construct_send_claims_transaction, decode_one_time_key,
    find_secret_input_index, CreateRequestTxnInputs, PrivateInputs, PublicInputSet, SpendingOutput,
    TransactionBuilderErrors, TransactionValidationError,
};
pub(crate) use zkplmt::core::*;
pub use zkplmt::{core::ZkPlmtError, models::*, verifiable_encryption};
// export test helpers under feature flag to prevent accidental use in production
#[cfg(feature = "test-helpers")]
pub use test_helpers::create_banned_members_list;

#[cfg(any(test, feature = "test-helpers"))]
pub use transactions::{generate_fake_input_txos, generate_spendable_utxos};

#[cfg(feature = "test-helpers")]
pub use transactions::{
    clear_redeem::TestClearRedeemRequestBuilder, verify_create_request_transaction,
    verify_exiting_redeem_request_transaction, verify_redeem_request_transaction,
    verify_send_claims_transaction, TestCreateRequestBuilder, TestExitingRedeemRequestBuilder,
    TestRedeemRequestBuilder, TestSendClaimsBuilder,
};
