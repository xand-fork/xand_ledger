use crate::{transactions::TransactionValidationError, TransactionOutput};
use serde::{Deserialize, Serialize};

/// Failures that may occur during transaction validation
#[derive(Debug, PartialEq, Eq, derive_more::From, Clone, Serialize, Deserialize)]
#[allow(clippy::large_enum_variant)]
#[cfg_attr(feature = "std", derive(thiserror::Error))]
pub enum FailureReason {
    #[cfg_attr(
        feature = "std",
        error("There weren't enough funds to complete the transactions")
    )]
    InsufficientFunds,
    #[cfg_attr(
        feature = "std",
        error("A pending create request already exists with the same correlation id")
    )]
    PendingCreateRequestIdAlreadyExists,
    #[cfg_attr(
        feature = "std",
        error("A pending redeem request already exists with the same correlation id")
    )]
    RedeemRequestIdAlreadyExists,
    #[cfg_attr(
        feature = "std",
        error("The correlation ID in the transaction already exists in the system")
    )]
    CorrelationIdCannotBeReused,
    #[cfg_attr(
        feature = "std",
        error("Could not verify that the transaction issuer is a valid participant")
    )]
    UnknownIdentity,
    #[cfg_attr(
        feature = "std",
        error("All claim create request amounts must be more than zero")
    )]
    CreateRequestAmountMustBeMoreThanZero,
    #[cfg_attr(
        feature = "std",
        error("A transaction output that would be created by the transaction already exists")
    )]
    OutputTxoAlreadyExists,
    #[cfg_attr(
        feature = "std",
        error("Transaction output was found but not in a spendable state")
    )]
    TxoNotSpendable(TransactionOutput),
    #[cfg_attr(
        feature = "std",
        error("The transaction failed validation for some underlying crypto reason")
    )]
    TransactionCouldNotBeVerified(#[cfg_attr(feature = "std", source)] TransactionValidationError),
    #[cfg_attr(
        feature = "std",
        error("No matching pending create request could be found while attempting to fulfill")
    )]
    NoMatchingCreateRequestToFulfill,
    #[cfg_attr(
        feature = "std",
        error("No matching pending create request could be found while attempting to cancel")
    )]
    NoMatchingCreateRequestToCancel,
    #[cfg_attr(
        feature = "std",
        error("Create requests must produce at least one output")
    )]
    CreateRequestMustProduceOutputs,
    #[cfg_attr(feature = "std", error("A transaction could not be dispatched: {0}"))]
    TransactionDispatchError(&'static str),
    #[cfg_attr(
        feature = "std",
        error("Trust account id is invalid (either not a valid ristretto point or unreachable)")
    )]
    InvalidTrustAccountId,
    #[cfg_attr(
        feature = "std",
        error("Key Image already spent, used to validate double spends")
    )]
    KeyImageAlreadySpent,
    #[cfg_attr(
        feature = "std",
        error("No matching redeem request could be found while attempting to fulfill")
    )]
    NoMatchingRedeemRequestToFulfill,
    #[cfg_attr(
        feature = "std",
        error("No matching redeem request could be found while attempting to cancel")
    )]
    NoMatchingRedeemRequestToCancel,
    #[cfg_attr(
        feature = "std",
        error("Exiting redeem request must be performed by an exiting member")
    )]
    ExitingRedeemRequestMustBeByExitingMember,
    #[cfg_attr(
        feature = "std",
        error("Exiting redeem requests can only spend TxOs issued before member was banned")
    )]
    ExitingRedeemRequestTxONewerThanBan,
    #[cfg_attr(feature = "std", error("Transaction output is not UETA compliant"))]
    TxoNotUETACompliant,
    #[cfg_attr(
        feature = "std",
        error(
            "A clear transaction output that would be consumed by the transaction is not redeemable"
        )
    )]
    ClearTxoNotRedeemable,
    #[cfg_attr(
        feature = "std",
        error(
            "A clear redeem request must have input clear transaction outputs such that their sum meets or exceeds the redeem amount"
        )
    )]
    InputLessThanRedeemAmount,
    #[cfg_attr(
        feature = "std",
        error(
            "A clear redeem request must have a change clear transaction output that has a value of the sum of the inputs minus the output"
        )
    )]
    TxoChangeOutputHasUnexpectedAmount,
    #[cfg_attr(
        feature = "std",
        error(
            "The clear transaction outputs of a clear redeem request must belong to the redeeming validator"
        )
    )]
    ClearTxoDoesNotBelongToRedeemingValidator,
}
