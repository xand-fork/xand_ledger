pub mod fakes;

use crate::{test_helpers::create_banned_members_list, PublicKey};
use fakes::FakeCsprng;
use proptest::prelude::*;
use rand::CryptoRng;

prop_compose! {
    pub(crate) fn arb_rng()(
        bytes: [u8; 32],
    ) -> FakeCsprng {
        FakeCsprng::from_seed(bytes)
    }
}

pub(crate) fn insert_index<R: RngCore + CryptoRng>(mut rng: R, array_len: usize) -> usize {
    if array_len > 0 {
        rng.gen_range(0, array_len)
    } else {
        0
    }
}

prop_compose! {
    pub(crate) fn banned_members(range: core::ops::Range<u8>)(
        rng in arb_rng(),
        count in range
    ) -> Vec<PublicKey> {
       create_banned_members_list(rng, count)
    }
}
