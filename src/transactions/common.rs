/// Common cryptographic functionality shared by creates, sends, and redeems
use crate::crypto::{get_G, get_L, LedgerCrypto};
use crate::{
    copy, create_zkplmt, hash_to_curve_point, shuffle,
    verifiable_encryption::VerifiableEncryptionOfCurvePoint, CurveVector, IdentityTag, KeyImage,
    MultiscalarMul, OpenedTransactionOutput, PrivateKey, Proof, PublicInputSet, PublicKey,
    RistrettoPoint, Scalar, TransactionBuilderErrors, TransactionInputSet, TransactionOutput,
    VectorTuple,
};
use alloc::vec::Vec;
use rand::{CryptoRng, RngCore};

#[cfg(any(test, feature = "test-helpers"))]
mod test_helpers;
#[cfg(any(test, feature = "test-helpers"))]
pub use test_helpers::*;

pub mod verification {
    use crate::{
        crypto::{get_G, get_L, LedgerCrypto},
        CurveVector, Proof, PublicInputSet, PublicKey, RistrettoPoint, Scalar,
        TransactionValidationError, VectorTuple, VerifiableEncryptionOfSignerKey,
    };
    use alloc::vec::Vec;
    use zkplmt::{bulletproofs::BulletRangeProof, core::curve25519_dalek::traits::Identity};

    pub fn verify_trust_sender_encryption(
        encrypted_sender: &VerifiableEncryptionOfSignerKey,
        trust_key: &PublicKey,
    ) -> Result<(), TransactionValidationError> {
        let G = get_G();

        let trust_vector = [CurveVector {
            x: G,
            y: (*trust_key).into(),
        }];

        let encrypted_sender = encrypted_sender.as_ref();
        encrypted_sender.verify(&trust_vector, &G)?;
        Ok(())
    }

    pub fn verify_alpha_proof<T: LedgerCrypto>(
        Z: RistrettoPoint,
        banned_members: &[PublicKey],
        Qs: &[RistrettoPoint],
        alpha: &Proof,
    ) -> Result<(), TransactionValidationError> {
        if banned_members.len() == Qs.len() {
            let mut values = vec![CurveVector { x: get_G(), y: Z }];

            let Ps_Qs: Vec<CurveVector> = banned_members
                .iter()
                .zip(Qs.iter())
                .map(|(P_e, Q_e)| CurveVector {
                    x: *P_e.as_ref(),
                    y: *Q_e,
                })
                .collect();

            values.extend(Ps_Qs);

            let alpha_tuple = VectorTuple { values };

            T::verify_zkplmt(&[], &[alpha_tuple], alpha)
                .map_err(|_| TransactionValidationError::InvalidAlpha)
        } else {
            Err(TransactionValidationError::BannedMemberListDifferentLengths)
        }
    }

    pub fn verify_commitment(
        r: Scalar,
        v: u64,
        commitment: &RistrettoPoint,
    ) -> Result<(), TransactionValidationError> {
        let G = get_G();
        let L = get_L();
        let expected_commit = r * G + Scalar::from(v) * L;

        (&expected_commit == commitment)
            .then_some(())
            .ok_or(TransactionValidationError::OpenedCommitmentMismatch)
    }

    pub fn verify_bullet_range_proof<
        'a,
        I: Iterator<Item = &'a RistrettoPoint>,
        T: LedgerCrypto,
    >(
        mut commitments: I,
        range_proof: &BulletRangeProof,
    ) -> Result<(), TransactionValidationError> {
        let mut range_proof_iter = range_proof.V.iter();
        // ensure commitments are aligned with range proofs
        while let (Some(commitment), range_commit) = (commitments.next(), range_proof_iter.next()) {
            if let Some(range_commit) = range_commit {
                if commitment != range_commit {
                    // this range proof corresponds to the wrong commitment
                    return Err(TransactionValidationError::RangeProofCommitmentMismatch);
                }
            } else {
                // There aren't enough range proofs to cover our value commitments
                // (there is no security risk to having excess range proofs)
                return Err(TransactionValidationError::RangeProofCommitmentMismatch);
            }
        }

        T::verify_bulletproof(range_proof)
            .then_some(())
            .ok_or(TransactionValidationError::InvalidBulletProof)
    }

    pub fn verify_sender_is_not_banned(
        inputs: &[PublicInputSet],
        sum_output: RistrettoPoint,
        Qs: &[RistrettoPoint],
    ) -> Result<(), TransactionValidationError> {
        let sum_inputs: Vec<RistrettoPoint> = inputs
            .iter()
            .map(|i| {
                i.txos
                    .components
                    .iter()
                    .map(|x| x.commitment())
                    .fold(RistrettoPoint::identity(), |X, Y| X + Y)
            })
            .collect();

        Qs.iter()
            .try_for_each(|Q| none_sent_by_banned_member(&sum_inputs, sum_output, *Q))
    }

    fn none_sent_by_banned_member(
        all_inputs: &[RistrettoPoint],
        outputs: RistrettoPoint,
        Q: RistrettoPoint,
    ) -> Result<(), TransactionValidationError> {
        all_inputs
            .iter()
            .try_for_each(|inputs| not_sent_by_banned_member(*inputs, outputs, Q))
    }

    fn not_sent_by_banned_member(
        inputs: RistrettoPoint,
        outputs: RistrettoPoint,
        Q: RistrettoPoint,
    ) -> Result<(), TransactionValidationError> {
        if inputs - outputs == Q {
            Err(TransactionValidationError::ConstructedByBannedMember)
        } else {
            Ok(())
        }
    }
}

pub(crate) struct ComponentInputs<'a> {
    pub(crate) Z: RistrettoPoint,
    pub(crate) key_images: &'a [KeyImage],
    pub(crate) sum_output: RistrettoPoint,
    pub(crate) input: &'a [TransactionOutput],
    pub(crate) hashes: &'a [RistrettoPoint],
    pub(crate) identity_input: &'a IdentityTag,
    pub(crate) identity_output: &'a IdentityTag,
    pub(crate) encryption: &'a VerifiableEncryptionOfCurvePoint,
}

// TODO: Needs a better name. I just dumbly extracted this to dedupe code. Parameters also need
//   better names where doable - or to be turned into a struct instead of a bunch of params.
pub(crate) fn components_to_vector_tuples(component_inputs: ComponentInputs) -> VectorTuple {
    let components = &component_inputs.input;
    let mut curve_vectors: Vec<CurveVector> = components
        .iter()
        .map(|c| CurveVector {
            x: c.public_key().x(),
            y: c.public_key().y(),
        })
        .collect();
    let mut hash_vectors = component_inputs
        .hashes
        .iter()
        .zip(component_inputs.key_images.iter())
        .map(|(hash, I)| CurveVector {
            x: *hash,
            y: (*I).into(),
        })
        .collect();
    curve_vectors.append(&mut hash_vectors);

    let sum_input = component_inputs
        .input
        .iter()
        .map(|c| c.commitment())
        .fold(RistrettoPoint::default(), |X, Y| X + Y);
    let sum_diff = sum_input - component_inputs.sum_output;

    curve_vectors.push(CurveVector {
        x: component_inputs.Z,
        y: sum_diff,
    });
    curve_vectors.push((*component_inputs.identity_input).into());
    curve_vectors.push((*component_inputs.identity_output).into());
    curve_vectors.push(CurveVector {
        x: component_inputs.encryption.Y,
        y: component_inputs.encryption.P_,
    });
    VectorTuple {
        values: curve_vectors,
    }
}

/// Takes a transaction input set and transforms the public keys of each input into a single
/// curve point in the output.
pub(crate) fn txn_input_keys_to_hash_curve_points(
    input: &[TransactionOutput],
) -> Vec<RistrettoPoint> {
    let list: Vec<RistrettoPoint> = input
        .iter()
        .map(|c| {
            let (A, B) = (c.public_key().x(), c.public_key().y());
            let Ac = A.compress();
            let Bc = B.compress();
            let A_bytes = Ac.as_bytes();
            let B_bytes = Bc.as_bytes();
            let mut bytes = [0u8; 64];
            copy(&mut bytes, A_bytes);
            copy(&mut bytes[32..], B_bytes);
            hash_to_curve_point(&bytes)
        })
        .collect();
    list
}

pub(crate) struct ShuffledInputs {
    pub(crate) inputs: Vec<PublicInputSet>,
    pub(crate) s_index: usize,
}

pub(crate) fn shuffle_inputs<R: RngCore + CryptoRng>(
    identity_input: IdentityTag,
    masking_inputs: Vec<PublicInputSet>,
    spends: &[OpenedTransactionOutput],
    rng: &mut R,
) -> ShuffledInputs {
    // First item in the inputs is the "real" one we can spend.
    let mut inputs = core::iter::once(PublicInputSet {
        txos: TransactionInputSet {
            components: spends.iter().map(|x| x.transaction_output).collect(),
        },
        identity_input,
    })
    .chain(masking_inputs.into_iter().map(|x| PublicInputSet {
        txos: TransactionInputSet {
            components: x.txos.components,
        },
        identity_input: x.identity_input,
    }))
    .collect::<Vec<_>>();
    let s_index = shuffle(&mut inputs, rng);

    ShuffledInputs { inputs, s_index }
}

pub(crate) fn public_inputs_to_set_of_hash_key_sets<T: LedgerCrypto>(
    inputs: &[PublicInputSet],
) -> Vec<Vec<RistrettoPoint>> {
    inputs
        .iter()
        .map(|input| T::txn_input_keys_to_hash_curve_points(&input.txos.components))
        .collect()
}

pub(crate) fn key_images(private_key: PrivateKey, hashes: &[RistrettoPoint]) -> Vec<KeyImage> {
    hashes
        .iter()
        .map(|hash| (private_key * hash).into())
        .collect()
}

pub(crate) fn sum_outputs(
    output_comms: &[RistrettoPoint],
    tx_inputs: &[TransactionOutput],
    private_key: PrivateKey,
    Z: RistrettoPoint,
) -> Result<RistrettoPoint, TransactionBuilderErrors> {
    let sum_output = output_comms.iter().sum::<RistrettoPoint>();
    let sum_input = tx_inputs
        .iter()
        .map(|x| x.commitment())
        .sum::<RistrettoPoint>();
    if sum_input - sum_output != private_key * Z {
        return Err(TransactionBuilderErrors::InputsDoNotMatchOutputValues);
    }
    Ok(sum_output)
}

pub(crate) fn output_blinding_factors<R: RngCore + CryptoRng>(
    count: usize,
    private_key: PrivateKey,
    private_input_spends: &[OpenedTransactionOutput],
    z: Scalar,
    rng: &mut R,
) -> Result<Vec<Scalar>, TransactionBuilderErrors> {
    // Randomness (blinding factors) for the Pedersen commitments of each output value.
    let mut output_blinding_factors = core::iter::repeat_with(|| Scalar::random(rng))
        .take(count)
        .collect::<Vec<_>>();
    let input_blinding_factor_sum = private_input_spends
        .iter()
        .map(|spend| spend.blinding_factor)
        .sum::<Scalar>();
    let diff = input_blinding_factor_sum - output_blinding_factors.iter().sum::<Scalar>();
    *output_blinding_factors
        .first_mut()
        .ok_or(TransactionBuilderErrors::NoSendClaimsOutputsProvided)? += diff - z * private_key;
    Ok(output_blinding_factors)
}

pub(crate) fn create_commitments(
    blinding_factors: &[Scalar],
    values: &[u64],
) -> Result<Vec<RistrettoPoint>, TransactionBuilderErrors> {
    if blinding_factors.len() != values.len() {
        return Err(TransactionBuilderErrors::BlindingFactorsAndValuesNotAligned);
    }

    Ok(blinding_factors
        .iter()
        .zip(values.iter())
        .map(|(r, v)| RistrettoPoint::multiscalar_mul([*r, Scalar::from(*v)], [get_G(), get_L()]))
        .collect::<Vec<_>>())
}

/// Find row in `inputs` that matches the given `key_images` and `private_key`.
pub fn find_secret_input_index(
    inputs: &[PublicInputSet],
    key_images: &[KeyImage],
    private_key: PrivateKey,
) -> Option<usize> {
    let inverted_private_key = private_key.invert();
    let target_h = key_images
        .iter()
        .map(|k| k.0 * inverted_private_key)
        .collect::<Vec<_>>();
    inputs
        .iter()
        .map(|set| txn_input_keys_to_hash_curve_points(&set.txos.components))
        .position(|h| h == target_h)
}

pub fn compute_banned_images_and_alpha<R: RngCore + CryptoRng>(
    banned_members: &[PublicKey],
    z: Scalar,
    Z: RistrettoPoint,
    rng: &mut R,
) -> (Vec<RistrettoPoint>, Proof) {
    let (Ps_Qs, Qs): (Vec<CurveVector>, Vec<RistrettoPoint>) = banned_members
        .iter()
        .map(|P_e| {
            let p = P_e.as_ref();
            let Q_e = z * p;
            (CurveVector { x: *p, y: Q_e }, Q_e)
        })
        .unzip();

    let mut values = vec![CurveVector { x: get_G(), y: Z }];

    values.extend(Ps_Qs);

    let alpha_tuple = VectorTuple { values };
    let alpha =
        create_zkplmt(&[], &[alpha_tuple], 0, z, rng).expect("This should never be reached");

    (Qs, alpha)
}

#[cfg(test)]
mod tests {
    use super::*;
    use core::iter::repeat_with;
    use rand::{rngs::OsRng, Rng};

    #[test]
    fn secret_input_index_can_be_found() {
        let rng = &mut OsRng::default();
        let private_key = PrivateKey::random(rng);
        let make_public_input_set = |rng: &mut OsRng| PublicInputSet {
            txos: TransactionInputSet {
                components: {
                    let count = rng.gen_range(1, 100);
                    repeat_with(|| TransactionOutput::random(rng))
                        .take(count)
                        .collect()
                },
            },
            identity_input: IdentityTag::random(rng),
        };
        let input_count = rng.gen_range(1, 100);
        let inputs = repeat_with(|| make_public_input_set(rng))
            .take(input_count)
            .collect::<Vec<_>>();
        let s_index = rng.gen_range(0, input_count);
        let key_images = inputs
            .iter()
            .skip(s_index)
            .map(|input| txn_input_keys_to_hash_curve_points(&input.txos.components))
            .next()
            .unwrap()
            .into_iter()
            .map(|hash| KeyImage::from(private_key * hash))
            .collect::<Vec<_>>();
        let found_s_index = find_secret_input_index(&inputs, &key_images, private_key);
        assert_eq!(found_s_index, Some(s_index));
    }
}
