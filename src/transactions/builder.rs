//! This module provides helpers for constructing transactions. It is only available if the `std`
//! flag is enabled, since transactions need not be constructed within the runtime.

use crate::{MonetaryValueError, ZkPlmtError};

#[derive(Debug, PartialEq, Eq)]
pub enum TransactionBuilderErrors {
    PrivateKeyBytesInvalid,
    ProblemFetchingIdentityTags,
    ProofConstructionFailure,
    IdentitySourceDoesNotMatchPrivateKey,
    MaxMaskingIdentitySourcesExceeded,
    ZeroValueInvalid,
    InvalidVerifiableEncryptionError(ZkPlmtError),
    NoSendClaimsOutputsProvided,
    InputsDoNotMatchOutputValues,
    EncryptionError,
    InsufficientRedeemRequestInput,
    BlindingFactorsAndValuesNotAligned,
    MonetaryValueError(MonetaryValueError),
}

impl From<ZkPlmtError> for TransactionBuilderErrors {
    fn from(e: ZkPlmtError) -> Self {
        TransactionBuilderErrors::InvalidVerifiableEncryptionError(e)
    }
}

impl From<MonetaryValueError> for TransactionBuilderErrors {
    fn from(e: MonetaryValueError) -> Self {
        TransactionBuilderErrors::MonetaryValueError(e)
    }
}

pub type Result<T, E = TransactionBuilderErrors> = core::result::Result<T, E>;
