#[cfg(any(test, feature = "test-helpers"))]
use crate::transactions::common::{
    generate_fake_input_txos, generate_spendable_utxos, random_private_key, TEST_METADATA_BYTES,
};
use crate::{
    create_zkplmt,
    crypto::{get_G, get_bases, DefaultCryptoImpl, LedgerCrypto},
    esig_payload::ESigPayload,
    model::PrivateRedeemRequestInputs,
    transactions::{
        common::{
            components_to_vector_tuples, key_images, output_blinding_factors, shuffle_inputs,
            verification::{verify_commitment, verify_trust_sender_encryption},
            ComponentInputs, ShuffledInputs,
        },
        compute_banned_images_and_alpha, create_commitments, public_inputs_to_set_of_hash_key_sets,
    },
    verifiable_encryption::VerifiableEncryptionOfCurvePoint,
    CoreRedeemRequestTransaction, CorrelationId, Encrypted, EncryptionKey, IdentityTag, OneTimeKey,
    OpenedTransactionOutput, PrivateKey, PublicInputSet, PublicKey, RedeemOutput,
    RedeemRequestTransaction, Scalar, SendClaimsOutput, SerializableForSigning,
    TransactionBuilderErrors, TransactionOutput, TransactionValidationError, VectorTuple,
};
#[cfg(any(test, feature = "test-helpers"))]
use alloc::rc::Rc;
use alloc::vec::Vec;
#[cfg(any(test, feature = "test-helpers"))]
use core::ops::Deref;
#[cfg(any(test, feature = "test-helpers"))]
use rand::Rng;
use rand::{CryptoRng, RngCore};
use zkplmt::bulletproofs::bullet_range_proof;

#[allow(clippy::too_many_arguments)]
pub fn construct_redeem_request_transaction<R, F>(
    private_inputs: PrivateRedeemRequestInputs,
    encryption_key: &EncryptionKey,
    redeem_amount: u64,
    trust_pub_key: PublicKey,
    correlation_id: CorrelationId,
    masking_inputs: Vec<PublicInputSet>,
    extra: Vec<u8>,
    banned_members: &[PublicKey],
    rng: &mut R,
    generate_metadata: F,
) -> Result<RedeemRequestTransaction, TransactionBuilderErrors>
where
    R: RngCore + CryptoRng,
    F: FnMut(
        &OpenedTransactionOutput,
        &EncryptionKey,
    ) -> Result<Encrypted, TransactionBuilderErrors>,
{
    let shuffled_inputs = shuffle_inputs(
        private_inputs.identity_input,
        masking_inputs,
        &private_inputs.spends,
        rng,
    );

    let CoreTransactionDetails {
        core_transaction, ..
    } = construct_core_redeem_request(
        private_inputs.private_key,
        encryption_key,
        redeem_amount,
        &private_inputs.spends,
        &shuffled_inputs,
        trust_pub_key,
        correlation_id,
        extra,
        banned_members,
        rng,
        generate_metadata,
    )?;

    let tuples = extract_tuples::<DefaultCryptoImpl>(&core_transaction);
    let buf = core_transaction.signable_bytes();
    let pi = create_zkplmt(
        &buf,
        &tuples,
        shuffled_inputs.s_index,
        private_inputs.private_key,
        rng,
    )?;
    Ok(RedeemRequestTransaction {
        core_transaction,
        pi,
    })
}

fn verify_txo_opened_data_is_correct(
    transaction: &RedeemRequestTransaction,
) -> Result<(), TransactionValidationError> {
    let opened_output = &transaction.core_transaction.output.redeem_output;
    verify_commitment(
        opened_output.blinding_factor,
        opened_output.value,
        opened_output.transaction_output.commitment(),
    )?;
    Ok(())
}

pub fn verify_redeem_request_transaction<T: LedgerCrypto>(
    transaction: &RedeemRequestTransaction,
    trust_key: &PublicKey,
    banned_members: &[PublicKey],
) -> Result<(), TransactionValidationError> {
    //Need to verify opened amount on redeem commitment
    verify_input_txo_set_length(transaction)?;
    verify_alpha_proof::<T>(transaction, banned_members)?;
    verify_sender_encryption(transaction, trust_key)?;
    verify_bullet_range_proof::<T>(transaction)?;
    verify_sender_is_not_banned(transaction)?;
    verify_txo_opened_data_is_correct(transaction)?;

    let tuples = extract_tuples::<T>(&transaction.core_transaction);
    let buf = transaction.core_transaction.signable_bytes();
    if T::verify_zkplmt(&buf, &tuples, &transaction.pi).is_err() {
        Err(TransactionValidationError::InvalidSignature)
    } else {
        Ok(())
    }
}

pub struct CoreTransactionDetails {
    core_transaction: CoreRedeemRequestTransaction,
    output_blinding_factors: Vec<Scalar>,
}

#[allow(clippy::too_many_arguments)]
fn construct_core_redeem_request<R, F>(
    private_key: PrivateKey,
    encryption_key: &EncryptionKey,
    redeem_amount: u64,
    private_input_spends: &[OpenedTransactionOutput],
    tx_inputs: &ShuffledInputs,
    trust_pub_key: PublicKey,
    correlation_id: CorrelationId,
    extra: Vec<u8>,
    banned_members: &[PublicKey],
    rng: &mut R,
    mut generate_metadata: F,
) -> Result<CoreTransactionDetails, TransactionBuilderErrors>
where
    R: RngCore + CryptoRng,
    F: FnMut(
        &OpenedTransactionOutput,
        &EncryptionKey,
    ) -> Result<Encrypted, TransactionBuilderErrors>,
{
    let G = get_G();
    let inputs_sum: u64 = private_input_spends.iter().map(|txo| txo.value).sum();
    let change_amount: u64 = inputs_sum
        .checked_sub(redeem_amount)
        .ok_or(TransactionBuilderErrors::InsufficientRedeemRequestInput)?;
    let output_values = [redeem_amount, change_amount];
    let z = Scalar::random(rng);
    let Z = z * G;
    let output_blinding_factors =
        output_blinding_factors(2, private_key, private_input_spends, z, rng)?;
    let output_comms = create_commitments(&output_blinding_factors, &output_values)?;
    let output_identity = IdentityTag::from_key_with_rand_base(private_key.into(), rng);
    let change_public_key = OneTimeKey::from_id_with_randomness(output_identity, rng);
    let redeem_public_key = OneTimeKey::from_id_with_randomness(output_identity, rng);
    let signer_public_key = PublicKey::from(private_key);
    // we don't need to include encrypted identity for change output since the signer already knows
    let encrypted_sender = VerifiableEncryptionOfCurvePoint::create(
        &signer_public_key.into(),
        &[IdentityTag::from_key_with_generator_base(trust_pub_key).into()],
        &G,
        rng,
    )?;

    let range_proof = bullet_range_proof(
        &output_blinding_factors[1..2],
        &output_values[1..2],
        get_bases(),
        rng,
    );

    let (Qs, alpha) = compute_banned_images_and_alpha(banned_members, z, Z, rng);
    let H = public_inputs_to_set_of_hash_key_sets::<DefaultCryptoImpl>(&tx_inputs.inputs);
    let key_images = key_images(private_key, &H[tx_inputs.s_index]);
    let change_output = OpenedTransactionOutput {
        transaction_output: TransactionOutput::new(
            output_comms[1],
            change_public_key,
            ESigPayload::new_with_ueta(),
        ),
        blinding_factor: output_blinding_factors[1],
        value: change_amount,
    };

    Ok(CoreTransactionDetails {
        core_transaction: CoreRedeemRequestTransaction {
            input: tx_inputs.inputs.clone(),
            output: RedeemOutput {
                redeem_output: OpenedTransactionOutput {
                    transaction_output: TransactionOutput::new(
                        output_comms[0],
                        redeem_public_key,
                        ESigPayload::new_with_ueta(),
                    ),
                    blinding_factor: output_blinding_factors[0],
                    value: redeem_amount,
                },
                change_output: SendClaimsOutput {
                    txo: change_output.transaction_output,
                    encrypted_metadata: generate_metadata(&change_output, encryption_key)?,
                },
            },
            output_identity,
            correlation_id,
            key_images,
            Z,
            alpha,
            range_proof,
            Qs,
            encrypted_sender: encrypted_sender.into(),
            extra,
        },
        output_blinding_factors,
    })
}

fn verify_input_txo_set_length(
    transaction: &RedeemRequestTransaction,
) -> Result<(), TransactionValidationError> {
    (!transaction.core_transaction.input.is_empty())
        .then_some(())
        .ok_or(TransactionValidationError::InvalidInputSet)
}

fn verify_alpha_proof<T: LedgerCrypto>(
    transaction: &RedeemRequestTransaction,
    banned_members: &[PublicKey],
) -> Result<(), TransactionValidationError> {
    crate::transactions::common::verification::verify_alpha_proof::<T>(
        transaction.core_transaction.Z,
        banned_members,
        &transaction.core_transaction.Qs,
        &transaction.core_transaction.alpha,
    )
}

fn verify_sender_encryption(
    transaction: &RedeemRequestTransaction,
    trust_key: &PublicKey,
) -> Result<(), TransactionValidationError> {
    verify_trust_sender_encryption(&transaction.core_transaction.encrypted_sender, trust_key)
}

fn verify_bullet_range_proof<T: LedgerCrypto>(
    transaction: &RedeemRequestTransaction,
) -> Result<(), TransactionValidationError> {
    let change = core::iter::once(
        transaction
            .core_transaction
            .output
            .change_output
            .txo
            .commitment(),
    );
    crate::transactions::common::verification::verify_bullet_range_proof::<_, T>(
        change,
        &transaction.core_transaction.range_proof,
    )
}

fn verify_sender_is_not_banned(
    transaction: &RedeemRequestTransaction,
) -> Result<(), TransactionValidationError> {
    let sum_output = transaction
        .core_transaction
        .output
        .redeem_output
        .transaction_output
        .commitment()
        + transaction
            .core_transaction
            .output
            .change_output
            .txo
            .commitment();
    crate::transactions::verification::verify_sender_is_not_banned(
        &transaction.core_transaction.input,
        sum_output,
        &transaction.core_transaction.Qs,
    )
}

fn extract_tuples<T: LedgerCrypto>(
    core_transaction: &CoreRedeemRequestTransaction,
) -> Vec<VectorTuple> {
    let H = public_inputs_to_set_of_hash_key_sets::<T>(&core_transaction.input);
    let key_images = &core_transaction.key_images;
    let output = &core_transaction.output;
    let output_sum = output.change_output.txo.commitment()
        + output.redeem_output.transaction_output.commitment();

    core_transaction
        .input
        .iter()
        .zip(&H)
        .map(|(input, hashes)| {
            let mut Y_j = components_to_vector_tuples(ComponentInputs {
                Z: core_transaction.Z,
                key_images,
                sum_output: output_sum,
                input: &input.txos.components,
                hashes,
                identity_input: &input.identity_input,
                identity_output: &core_transaction.output_identity,
                encryption: (&core_transaction.encrypted_sender).as_ref(),
            });
            // add the change and redeem output public key
            Y_j.values.push(
                core_transaction
                    .output
                    .change_output
                    .txo
                    .public_key()
                    .into(),
            );
            Y_j.values.push(
                core_transaction
                    .output
                    .redeem_output
                    .transaction_output
                    .public_key()
                    .into(),
            );
            Y_j
        })
        .collect::<Vec<_>>()
}

/// Builder pattern for redeem request transactions
#[cfg(any(test, feature = "test-helpers"))]
#[derive(Clone)]
pub struct TestRedeemRequestBuilder {
    input_identity: Option<IdentityTag>,
    input_values: Vec<u64>,
    redeem_amount: u64,
    private_key: Option<Scalar>,
    encryption_key: Option<EncryptionKey>,
    correlation_id: Option<CorrelationId>,
    trust_public_key: PublicKey,
    banned_members: Option<Vec<PublicKey>>,
    extra: Vec<u8>,
    generate_metadata: Rc<
        dyn Fn(
            OpenedTransactionOutput,
            EncryptionKey,
        ) -> Result<Encrypted, TransactionBuilderErrors>,
    >,
}

#[cfg(any(test, feature = "test-helpers"))]
pub struct RedeemRequestInputs {
    pub private_inputs: PrivateRedeemRequestInputs,
    pub encryption_key: EncryptionKey,
    pub redeem_amount: u64,
    pub trust_pub_key: PublicKey,
    pub correlation_id: CorrelationId,
    pub decoy_txos: Vec<PublicInputSet>,
    pub extra: Vec<u8>,
    pub banned_members: Vec<PublicKey>,
}

#[cfg(any(test, feature = "test-helpers"))]
pub struct RedeemRequestBuildResponse {
    pub redeem_request: RedeemRequestTransaction,
    pub trust_pub_key: PublicKey,
}

#[cfg(any(test, feature = "test-helpers"))]
pub struct UnsignedRedeemRequest {
    pub secret_index: usize,
    pub private_key: PrivateKey,
    pub core_details: CoreTransactionDetails,
    pub trust_pub_key: PublicKey,
}

#[cfg(any(test, feature = "test-helpers"))]
impl UnsignedRedeemRequest {
    pub fn sign<R>(self, mut rng: R) -> RedeemRequestTransaction
    where
        R: RngCore + CryptoRng,
    {
        let tuples = extract_tuples::<DefaultCryptoImpl>(&self.core_details.core_transaction);
        let buf = &self.core_details.core_transaction.signable_bytes();
        let pi =
            create_zkplmt(buf, &tuples, self.secret_index, self.private_key, &mut rng).unwrap();
        RedeemRequestTransaction {
            core_transaction: self.core_details.core_transaction,
            pi,
        }
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl Default for TestRedeemRequestBuilder {
    fn default() -> Self {
        TestRedeemRequestBuilder {
            input_identity: None,
            input_values: vec![5, 5, 5],
            redeem_amount: 11,
            private_key: None,
            encryption_key: None,
            correlation_id: None,
            trust_public_key: Default::default(),
            banned_members: None,
            extra: vec![],
            generate_metadata: Rc::new(|_, _| Ok(TEST_METADATA_BYTES.to_vec().into())),
        }
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl TestRedeemRequestBuilder {
    /// Specify identity source for transaction
    pub fn with_identity_source(mut self, identity_source: IdentityTag) -> Self {
        self.input_identity = Some(identity_source);
        self
    }

    pub fn with_redeem_amount(mut self, amount: u64) -> Self {
        self.redeem_amount = amount;
        self
    }

    /// Specify values for transaction
    pub fn input_values(mut self, values: Vec<u64>) -> Self {
        self.input_values = values;
        self
    }

    /// Specify private key of transaction creator
    pub fn with_private_key(mut self, private_key: Scalar) -> Self {
        self.private_key = Some(private_key);
        self
    }

    pub fn with_encryption_key(mut self, encryption_key: EncryptionKey) -> Self {
        self.encryption_key = Some(encryption_key);
        self
    }

    /// Specify association id for cash transfer
    pub fn correlation_id(mut self, correlation_id: CorrelationId) -> Self {
        self.correlation_id = Some(correlation_id);
        self
    }

    /// Specify the trust public key
    pub fn trust_public_key(mut self, trust_public_key: PublicKey) -> Self {
        self.trust_public_key = trust_public_key;
        self
    }

    /// Include extra metadata with transaction
    pub fn extra(mut self, extra: Vec<u8>) -> Self {
        self.extra = extra;
        self
    }

    pub fn with_banned_members(self, banned_members: Vec<PublicKey>) -> Self {
        Self {
            banned_members: Some(banned_members),
            ..self
        }
    }

    pub fn with_generate_metadata(
        self,
        metadata: Rc<
            dyn Fn(
                OpenedTransactionOutput,
                EncryptionKey,
            ) -> Result<Encrypted, TransactionBuilderErrors>,
        >,
    ) -> Self {
        Self {
            generate_metadata: metadata,
            ..self
        }
    }

    fn redeem_request_inputs<R>(self, mut rng: R) -> RedeemRequestInputs
    where
        R: RngCore + CryptoRng,
    {
        let private_key = self
            .private_key
            .unwrap_or_else(|| random_private_key(&mut rng));

        let my_identity_tag = self
            .input_identity
            .unwrap_or_else(|| IdentityTag::from_key_with_rand_base(private_key.into(), &mut rng));
        let my_spendable_txos =
            generate_spendable_utxos(my_identity_tag, self.input_values, &mut rng);
        let decoy_txos = generate_fake_input_txos(4, my_spendable_txos.len(), &mut rng);

        let correlation_id = self.correlation_id.unwrap_or_else(|| rng.gen());
        let private_inputs = PrivateRedeemRequestInputs {
            spends: my_spendable_txos,
            identity_input: my_identity_tag,
            private_key,
        };
        let banned_members = self.banned_members.unwrap_or_default();
        let encryption_key = if let Some(key) = self.encryption_key {
            key
        } else {
            EncryptionKey::random(&mut rng)
        };
        RedeemRequestInputs {
            private_inputs,
            encryption_key,
            redeem_amount: self.redeem_amount,
            trust_pub_key: self.trust_public_key,
            correlation_id,
            decoy_txos,
            extra: self.extra,
            banned_members,
        }
    }

    pub fn build_unsigned<R>(self, mut rng: R) -> UnsignedRedeemRequest
    where
        R: RngCore + CryptoRng,
    {
        let generate_metadata = self.generate_metadata.clone();

        let inputs = self.redeem_request_inputs(&mut rng);
        let shuffled_inputs = shuffle_inputs(
            inputs.private_inputs.identity_input,
            inputs.decoy_txos,
            &inputs.private_inputs.spends,
            &mut rng,
        );

        let core_transaction = construct_core_redeem_request(
            inputs.private_inputs.private_key,
            &inputs.encryption_key,
            inputs.redeem_amount,
            &inputs.private_inputs.spends,
            &shuffled_inputs,
            inputs.trust_pub_key,
            inputs.correlation_id,
            inputs.extra,
            &inputs.banned_members,
            &mut rng,
            move |txo, key| generate_metadata.deref()(txo.clone(), *key),
        )
        .unwrap();

        UnsignedRedeemRequest {
            secret_index: shuffled_inputs.s_index,
            private_key: inputs.private_inputs.private_key,
            core_details: core_transaction,
            trust_pub_key: inputs.trust_pub_key,
        }
    }

    pub fn build<R>(self, mut csprng: R) -> RedeemRequestBuildResponse
    where
        R: RngCore + CryptoRng,
    {
        let generate_metadata = self.generate_metadata.clone();
        let inputs = self.redeem_request_inputs(&mut csprng);
        let redeem_request = construct_redeem_request_transaction(
            inputs.private_inputs,
            &inputs.encryption_key,
            inputs.redeem_amount,
            inputs.trust_pub_key,
            inputs.correlation_id,
            inputs.decoy_txos,
            inputs.extra,
            &inputs.banned_members,
            &mut csprng,
            move |txo, key| generate_metadata.deref()(txo.clone(), *key),
        )
        .unwrap();
        RedeemRequestBuildResponse {
            redeem_request,
            trust_pub_key: inputs.trust_pub_key,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        crypto::{get_L, DefaultCryptoImpl},
        test::{arb_rng, banned_members, insert_index},
    };
    use proptest::prelude::*;
    use rand::rngs::OsRng;

    #[test]
    fn happy_path() {
        let rng = OsRng::default();
        let builder = TestRedeemRequestBuilder::default();
        let redeem_request = builder.clone().build(rng).redeem_request;
        assert!(verify_redeem_request_transaction::<DefaultCryptoImpl>(
            &redeem_request,
            &builder.trust_public_key,
            &[]
        )
        .is_ok())
    }

    // This test attempts to redeem more than the input amount by simply replacing the burn
    // commitment with a different one that has an amount higher than the inputs.
    #[test]
    fn redeem_more_than_input_fails() {
        let rng = OsRng::default();
        let builder = TestRedeemRequestBuilder::default();
        let mut unsigned_redeem_request = builder.clone().build_unsigned(rng);

        // preserve blinding factor used in original zero-value proof
        let original = unsigned_redeem_request
            .core_details
            .core_transaction
            .output
            .redeem_output;
        // construct valid redeem output commitment higher than the inputs
        let value: u64 = 100_000_000;
        unsigned_redeem_request
            .core_details
            .core_transaction
            .output
            .redeem_output = OpenedTransactionOutput {
            transaction_output: TransactionOutput::with_compliant_esig_payload(
                original.blinding_factor * get_G() + Scalar::from(value) * get_L(),
                *original.transaction_output.public_key(),
            ),
            blinding_factor: original.blinding_factor,
            value: 100_000_000,
        };

        // sign redeem request
        let redeem_request = unsigned_redeem_request.sign(rng);
        let result = verify_redeem_request_transaction::<DefaultCryptoImpl>(
            &redeem_request,
            &builder.trust_public_key,
            &[],
        );
        // Verify (Z, sum_diff) is no longer parallel to p
        assert!(
            matches!(result, Err(TransactionValidationError::InvalidSignature)),
            "Expected InvalidSignature but got {:?}",
            result
        );
    }

    // This test attempts to transfer the change of a redeem output to a different owner
    // (e.g. a send claims transaction)
    // To verify this malicious proof, modify extract_tuples to exclude the change public key
    #[test]
    fn change_output_must_be_owned_by_issuer() {
        let mut rng = OsRng::default();
        let builder = TestRedeemRequestBuilder::default();
        let trust_key = builder.trust_public_key;
        let mut unsigned_redeem_request = builder.build_unsigned(rng);

        let txo = TransactionOutput::with_compliant_esig_payload(
            *unsigned_redeem_request
                .core_details
                .core_transaction
                .output
                .change_output
                .txo
                .commitment(),
            OneTimeKey::random(&mut rng),
        );

        unsigned_redeem_request
            .core_details
            .core_transaction
            .output
            .change_output
            .txo = txo;

        // sign redeem request
        let redeem_request = unsigned_redeem_request.sign(rng);
        let result = verify_redeem_request_transaction::<DefaultCryptoImpl>(
            &redeem_request,
            &trust_key,
            &[],
        );
        // Verify (A_c, B_c) is no longer parallel to p
        assert!(
            matches!(result, Err(TransactionValidationError::InvalidSignature)),
            "Expected InvalidSignature but got {:?}",
            result
        );
    }

    // This test attempts to transfer the redeem output to a different owner in the case of a canceled
    // redeem request. (e.g. using redeem to send claims)
    // To verify this malicious proof, modify extract_tuples to exclude the redeem public key
    #[test]
    fn redeem_output_must_be_owned_by_issuer() {
        let mut rng = OsRng::default();
        let builder = TestRedeemRequestBuilder::default();
        let mut unsigned_redeem_request = builder.clone().build_unsigned(rng);
        let txo = TransactionOutput::with_compliant_esig_payload(
            *unsigned_redeem_request
                .core_details
                .core_transaction
                .output
                .redeem_output
                .transaction_output
                .commitment(),
            OneTimeKey::random(&mut rng),
        );

        unsigned_redeem_request
            .core_details
            .core_transaction
            .output
            .redeem_output
            .transaction_output = txo;

        // sign redeem request
        let redeem_request = unsigned_redeem_request.sign(rng);
        let result = verify_redeem_request_transaction::<DefaultCryptoImpl>(
            &redeem_request,
            &builder.trust_public_key,
            &[],
        );
        // Verify (A_c, B_c) is no longer parallel to p
        assert!(
            matches!(result, Err(TransactionValidationError::InvalidSignature)),
            "Expected InvalidSignature but got {:?}",
            result
        );
    }

    // This test attempts to overflow the concealed change amount
    // in order to redeem arbitrary amounts.
    //
    // To see this attack work, disable `verify_bullet_range_proof`
    #[test]
    fn cannot_overflow_output_commitments() {
        let rng = OsRng::default();
        let builder = TestRedeemRequestBuilder::default();
        let mut unsigned_redeem_request = builder.clone().build_unsigned(rng);

        let input_sum: u64 = builder.input_values.iter().sum();

        // preserve blinding factor used in original zero-value proof
        let original = unsigned_redeem_request
            .core_details
            .core_transaction
            .output
            .redeem_output;
        // construct valid redeem output commitment higher than the inputs
        let redeem_value = 100_000_000u64;
        unsigned_redeem_request
            .core_details
            .core_transaction
            .output
            .redeem_output = OpenedTransactionOutput {
            transaction_output: TransactionOutput::with_compliant_esig_payload(
                *original.transaction_output.commitment(),
                *original.transaction_output.public_key(),
            ),
            blinding_factor: original.blinding_factor,
            value: redeem_value,
        };

        // preserve blinding factor used in original zero-value proof
        let change_r = unsigned_redeem_request
            .core_details
            .output_blinding_factors
            .get(1)
            .unwrap();

        // construct change output that overflows burn commitment back to 0
        let change_value = Scalar::from(input_sum) - Scalar::from(redeem_value);
        let change_commitment = change_r * get_G() + change_value * get_L();

        unsigned_redeem_request
            .core_details
            .core_transaction
            .output
            .change_output = SendClaimsOutput {
            txo: TransactionOutput::with_compliant_esig_payload(
                change_commitment,
                *unsigned_redeem_request
                    .core_details
                    .core_transaction
                    .output
                    .change_output
                    .txo
                    .public_key(),
            ),
            encrypted_metadata: Default::default(),
        };

        // Change bullet proof commitment to match overflowed change
        *unsigned_redeem_request
            .core_details
            .core_transaction
            .range_proof
            .V
            .get_mut(0)
            .unwrap() = change_commitment;

        // sign redeem request
        let redeem_request = unsigned_redeem_request.sign(rng);
        let verify_result = verify_redeem_request_transaction::<DefaultCryptoImpl>(
            &redeem_request,
            &builder.trust_public_key,
            &[],
        );
        assert!(matches!(
            verify_result,
            Err(TransactionValidationError::InvalidBulletProof)
        ))
    }

    // This test attempts to redeem more than the committed amount.
    // While the proofs work against the commitment of the redeemed amount,
    // the trust will redeem an amount based on the opened redeem amount.
    // This test ensures that the inputs for the redeem commitment
    // can't deviate from the values used in the proofs.
    //
    // To see this attack work, disable `verify_burn_commitment_is_correct`
    #[test]
    fn opened_redeem_amount_cannot_be_different_than_commitment() {
        let rng = OsRng::default();
        let builder = TestRedeemRequestBuilder::default();
        let mut unsigned_redeem_request = builder.clone().build_unsigned(rng);
        // modify opened burn commitment inputs
        unsigned_redeem_request
            .core_details
            .core_transaction
            .output
            .redeem_output
            .value = 100_000_000;

        // sign redeem request
        let redeem_request = unsigned_redeem_request.sign(rng);

        // Verify commitment mismatch error
        assert!(matches!(
            verify_redeem_request_transaction::<DefaultCryptoImpl>(
                &redeem_request,
                &builder.trust_public_key,
                &[]
            ),
            Err(TransactionValidationError::OpenedCommitmentMismatch)
        ))
    }

    // Prove that the encryption recipient is verified to be trust pub key
    //
    // To see this attack work, disable `verify_sender_encryption`
    #[test]
    fn cant_encrypt_sender_identity_to_non_trust_key() {
        let mut rng = OsRng::default();
        let builder = TestRedeemRequestBuilder::default();
        let mut unsigned_redeem_request = builder.clone().build_unsigned(rng);

        // replace encryption recipient with sender instead of trust
        unsigned_redeem_request
            .core_details
            .core_transaction
            .encrypted_sender = VerifiableEncryptionOfCurvePoint::create(
            &PublicKey::from(unsigned_redeem_request.private_key).into(),
            &[IdentityTag::from_key_with_generator_base(PublicKey::from(
                unsigned_redeem_request.private_key,
            ))
            .into()],
            &get_G(),
            &mut rng,
        )
        .unwrap()
        .into();

        // sign redeem request
        let redeem_request = unsigned_redeem_request.sign(rng);

        // Verify proofs expect the trust to be the encryption recipient
        assert!(matches!(
            verify_redeem_request_transaction::<DefaultCryptoImpl>(
                &redeem_request,
                &builder.trust_public_key,
                &[]
            ),
            Err(TransactionValidationError::InvalidEncryption(_))
        ))
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn can_redeem_as_non_banned_member(
            banned_members in banned_members(0..10),
            rng in arb_rng()
        ) {
            let builder = TestRedeemRequestBuilder::default()
                .with_banned_members(banned_members.clone());
            let transaction = builder.clone().build(rng).redeem_request;
            assert!(verify_redeem_request_transaction::<DefaultCryptoImpl>(&transaction, &builder.trust_public_key, &banned_members).is_ok());
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn cannot_verify_redeem_with_wrong_banned_member(
            mut banned_members in banned_members(1..10),
            mut rng in arb_rng()
        ) {
            let builder = TestRedeemRequestBuilder::default()
                .with_banned_members(banned_members.clone());
            let transaction = builder.clone().build(&mut rng).redeem_request;

            // Replace one banned member
            let some_private_key = random_private_key(&mut rng);
            let some_public_key = (some_private_key * get_G()).into();
            let replace_index = insert_index(rng, banned_members.len());
            banned_members[replace_index] = some_public_key;

            assert!(matches!(
                verify_redeem_request_transaction::<DefaultCryptoImpl>(&transaction, &builder.trust_public_key, &banned_members),
                Err(TransactionValidationError::InvalidAlpha)
            ));
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn banned_member_list_cannot_be_different_length_than_Qs(
            mut banned_members in banned_members(0..10),
            mut rng in arb_rng()
        ) {
            let builder = TestRedeemRequestBuilder::default()
                .with_banned_members(banned_members.clone());
            let transaction = builder.clone().build(&mut rng).redeem_request;

            // Add one banned member
            let some_private_key = random_private_key(&mut rng);
            let some_public_key = (some_private_key * get_G()).into();
            let replace_index = insert_index(rng, banned_members.len());
            banned_members.push(some_public_key);

            assert!(matches!(
                verify_redeem_request_transaction::<DefaultCryptoImpl>(&transaction, &builder.trust_public_key, &banned_members),
                Err(TransactionValidationError::BannedMemberListDifferentLengths)
            ));
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn cannot_redeem_as_banned_member(
            mut banned_members in banned_members(0..10),
            mut rng in arb_rng()
        ) {
            let own_private_key = random_private_key(&mut rng);
            let own_public_key = (own_private_key * get_G()).into();
            let insert_index = insert_index(&mut rng, banned_members.len());
            banned_members.insert(insert_index, own_public_key);

            let builder = TestRedeemRequestBuilder::default()
                .with_private_key(own_private_key)
                .with_banned_members(banned_members.clone());
            let transaction = builder.clone().build(rng).redeem_request;
            assert!(matches!(
                verify_redeem_request_transaction::<DefaultCryptoImpl>(&transaction, &builder.trust_public_key, &banned_members),
                Err(TransactionValidationError::ConstructedByBannedMember)
            ));
        }
    }
}
