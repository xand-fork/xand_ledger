use super::*;
#[cfg(any(test, feature = "test-helpers"))]
use crate::transactions::common::{
    generate_fake_input_txos, generate_spendable_utxos, random_private_key, random_public_key,
    TEST_METADATA_BYTES,
};
use crate::{
    crypto::{get_G, get_bases, DefaultCryptoImpl, LedgerCrypto},
    esig_payload::ESigPayload,
    transactions::common::{
        components_to_vector_tuples, key_images, output_blinding_factors, shuffle_inputs,
        sum_outputs, ComponentInputs, ShuffledInputs,
    },
    verifiable_encryption::VerifiableEncryptionOfCurvePoint,
    SerializableForSigning, VectorTuple,
};
use curve25519_dalek::ristretto::RistrettoPoint;
use zkplmt::{bulletproofs::bullet_range_proof, core::create_zkplmt};

pub fn construct_send_claims_transaction<R, F>(
    private_inputs: PrivateInputs,
    masking_inputs: Vec<PublicInputSet>,
    private_key: PrivateKey,
    banned_members: Vec<PublicKey>,
    rng: &mut R,
    generate_metadata: F,
) -> Result<SendClaimsTransaction>
where
    R: RngCore + CryptoRng,
    F: FnMut(&OpenedTransactionOutput, &EncryptionKey) -> Result<Encrypted>,
{
    let shuffled_inputs = shuffle_inputs(
        private_inputs.identity_input,
        masking_inputs,
        &private_inputs.spends,
        rng,
    );

    let core_transaction = construct_core_send_claims_transaction(
        private_key,
        &private_inputs.outputs,
        &private_inputs.spends,
        &shuffled_inputs,
        banned_members,
        generate_metadata,
        rng,
    )?;

    let tuples = extract_tuples_from_send_claims::<DefaultCryptoImpl>(&core_transaction);
    let buf = core_transaction.signable_bytes();
    let pi = create_zkplmt(&buf, &tuples, shuffled_inputs.s_index, private_key, rng)?;
    Ok(SendClaimsTransaction {
        core_transaction,
        pi,
    })
}

fn construct_core_send_claims_transaction<R, F>(
    private_key: PrivateKey,
    outputs: &[SpendingOutput],
    private_input_spends: &[OpenedTransactionOutput],
    tx_inputs: &ShuffledInputs,
    banned_members: Vec<PublicKey>,
    mut generate_metadata: F,
    rng: &mut R,
) -> Result<CoreSendClaimsTransaction, TransactionBuilderErrors>
where
    R: RngCore + CryptoRng,
    F: FnMut(&OpenedTransactionOutput, &EncryptionKey) -> Result<Encrypted>,
{
    let G = get_G();
    let output_values = outputs.iter().map(|x| x.value).collect::<Vec<_>>();
    // z is the randomness used to obfuscate which sum of the inputs equal the output
    let z = Scalar::random(rng);
    let Z = z * G;
    let output_blinding_factors =
        output_blinding_factors(outputs.len(), private_key, private_input_spends, z, rng)?;
    let output_comms = create_commitments(&output_blinding_factors, &output_values)?;
    let output_sum = sum_outputs(
        &output_comms,
        &tx_inputs.inputs[tx_inputs.s_index].txos.components,
        private_key,
        Z,
    )?;
    let identity_output = IdentityTag::from_key_with_rand_base(private_key.into(), rng);
    let output_public_keys = outputs
        .iter()
        .enumerate()
        .map(|(i, x)| derive_one_time_key(x.key, Message::new(identity_output), i, private_key))
        .collect::<Vec<_>>();
    let output_public_keys_AB = output_public_keys.iter().map(|k| k.0).collect::<Vec<_>>();
    let signer_public_perm_P = private_key * get_G();
    let encrypted_sender = VerifiableEncryptionOfCurvePoint::create(
        &signer_public_perm_P,
        &output_public_keys_AB,
        &G,
        rng,
    )?;
    let outputs = output_comms
        .into_iter()
        .zip(output_public_keys)
        .zip(output_blinding_factors.iter().copied())
        .zip(output_values.iter().copied())
        .zip(outputs.iter())
        .map(|((((comm, key), r), v), spending_output)| {
            make_send_claims_output(
                comm,
                key,
                r,
                v,
                &spending_output.encryption_key,
                &mut generate_metadata,
            )
        })
        .collect::<Result<_, TransactionBuilderErrors>>()?;

    let (Qs, alpha) = compute_banned_images_and_alpha(&banned_members, z, Z, rng);
    let H = public_inputs_to_set_of_hash_key_sets::<DefaultCryptoImpl>(&tx_inputs.inputs);
    let key_images = key_images(private_key, &H[tx_inputs.s_index]);
    let range_proof =
        bullet_range_proof(&output_blinding_factors, &output_values, get_bases(), rng);
    Ok(CoreSendClaimsTransaction {
        input: tx_inputs.inputs.clone(),
        output: outputs,
        output_identity: identity_output,
        key_images,
        Z,
        alpha,
        range_proof,
        encrypted_sender: encrypted_sender.into(),
        Qs,
    })
}

fn extract_tuples_from_send_claims<T: LedgerCrypto>(
    core_transaction: &CoreSendClaimsTransaction,
) -> Vec<VectorTuple> {
    let H = public_inputs_to_set_of_hash_key_sets::<T>(&core_transaction.input);
    let key_images = &core_transaction.key_images;
    let output_sum = core_transaction
        .output
        .iter()
        .map(|output| output.txo.commitment())
        .sum();
    core_transaction
        .input
        .iter()
        .zip(&H)
        .map(|(input, hashes)| {
            components_to_vector_tuples(ComponentInputs {
                Z: core_transaction.Z,
                key_images,
                sum_output: output_sum,
                input: &input.txos.components,
                hashes,
                identity_input: &input.identity_input,
                identity_output: &core_transaction.output_identity,
                encryption: (&core_transaction.encrypted_sender).as_ref(),
            })
        })
        .collect::<Vec<_>>()
}

fn make_send_claims_output<F>(
    comm: RistrettoPoint,
    key: OneTimeKey,
    blinding_factor: Scalar,
    value: u64,
    encryption_key: &EncryptionKey,
    mut generate_metadata: F,
) -> Result<SendClaimsOutput, TransactionBuilderErrors>
where
    F: FnMut(&OpenedTransactionOutput, &EncryptionKey) -> Result<Encrypted>,
{
    let txo = TransactionOutput::new(comm, key, ESigPayload::new_with_ueta());
    let opened_txo = OpenedTransactionOutput {
        transaction_output: txo,
        blinding_factor,
        value,
    };
    Ok(SendClaimsOutput {
        txo,
        encrypted_metadata: generate_metadata(&opened_txo, encryption_key)?,
    })
}

/// Verifies a send claims transaction and its proofs. Requires a list of banned members is included. The list of
/// banned members must be the same length and same order as the Qs included on the transaction.
pub fn verify_send_claims_transaction<T: LedgerCrypto>(
    transaction: &SendClaimsTransaction,
    banned_members: Vec<PublicKey>,
) -> Result<(), TransactionValidationError> {
    // ensure the txo set aligns with the identity tags
    verify_input_txo_set_length(transaction)?;
    // verify input value == output value
    verify_alpha_proof::<T>(transaction, &banned_members)?;
    // verify the proof from the sender that their identity is properly encrypted to the receiver.
    verify_encrypted_sender(transaction)?;
    // verify the bullet range proof that all inputs are > 0
    verify_bullet_range_proof::<T>(transaction)?;
    // verify the sender of send claims transaction is not among banned members
    verify_sender_is_not_banned(transaction)?;

    // Setup for the primary proof, pi
    let tuples = extract_tuples_from_send_claims::<T>(&transaction.core_transaction);
    let buf = transaction.core_transaction.signable_bytes();
    if T::verify_zkplmt(&buf, &tuples, &transaction.pi).is_err() {
        Err(TransactionValidationError::InvalidSignature)
    } else {
        Ok(())
    }
}

fn verify_input_txo_set_length(
    transaction: &SendClaimsTransaction,
) -> Result<(), TransactionValidationError> {
    (!transaction.core_transaction.input.is_empty())
        .then_some(())
        .ok_or(TransactionValidationError::InvalidInputSet)
}

fn verify_alpha_proof<T: LedgerCrypto>(
    transaction: &SendClaimsTransaction,
    banned_members: &[PublicKey],
) -> Result<(), TransactionValidationError> {
    crate::transactions::common::verification::verify_alpha_proof::<T>(
        transaction.core_transaction.Z,
        banned_members,
        &transaction.core_transaction.Qs,
        &transaction.core_transaction.alpha,
    )
}

fn verify_encrypted_sender(
    transaction: &SendClaimsTransaction,
) -> Result<(), TransactionValidationError> {
    let target_ephemeral_pub_A_B: Vec<CurveVector> = transaction
        .core_transaction
        .output
        .iter()
        .map(|output| output.txo.public_key().0)
        .collect();

    let encrypted_sender: VerifiableEncryptionOfCurvePoint =
        transaction.core_transaction.encrypted_sender.clone().into();

    encrypted_sender.verify(target_ephemeral_pub_A_B.as_slice(), &get_G())?;
    Ok(())
}

fn verify_bullet_range_proof<T: LedgerCrypto>(
    transaction: &SendClaimsTransaction,
) -> Result<(), TransactionValidationError> {
    let commitment_iter = transaction
        .core_transaction
        .output
        .iter()
        .map(|output| output.txo.commitment());

    crate::transactions::common::verification::verify_bullet_range_proof::<_, T>(
        commitment_iter,
        &transaction.core_transaction.range_proof,
    )
}

fn verify_sender_is_not_banned(
    transaction: &SendClaimsTransaction,
) -> Result<(), TransactionValidationError> {
    let sum_output = transaction
        .core_transaction
        .output
        .iter()
        .map(|output| output.txo.commitment())
        .fold(RistrettoPoint::default(), |X, Y| X + Y);
    crate::transactions::verification::verify_sender_is_not_banned(
        &transaction.core_transaction.input,
        sum_output,
        &transaction.core_transaction.Qs,
    )
}

#[cfg(any(test, feature = "test-helpers"))]
pub struct TestSendClaimsBuilder {
    input_values: Vec<u64>,
    output_values: Vec<u64>,
    id_source: Option<IdentityTag>,
    private_key: Option<PrivateKey>,
    banned_members: Option<Vec<PublicKey>>,
}

#[cfg(any(test, feature = "test-helpers"))]
impl TestSendClaimsBuilder {
    pub fn with_inputs(self, inputs: Vec<u64>) -> Self {
        Self {
            input_values: inputs,
            ..self
        }
    }

    pub fn with_outputs(self, outputs: Vec<u64>) -> Self {
        Self {
            output_values: outputs,
            ..self
        }
    }

    pub fn with_identity_source(self, id: IdentityTag) -> Self {
        Self {
            id_source: Some(id),
            ..self
        }
    }

    pub fn with_private_key(self, private_key: PrivateKey) -> Self {
        Self {
            private_key: Some(private_key),
            ..self
        }
    }

    pub fn with_banned_members(self, banned_members: Vec<PublicKey>) -> Self {
        Self {
            banned_members: Some(banned_members),
            ..self
        }
    }

    pub fn build_result<R>(self, mut csprng: R) -> Result<SendClaimsTransaction>
    where
        R: RngCore + CryptoRng,
    {
        let private_key = self
            .private_key
            .unwrap_or_else(|| random_private_key(&mut csprng));

        let recipient = random_public_key(&mut csprng);

        let my_identity_tag = self.id_source.unwrap_or_else(|| {
            IdentityTag::from_key_with_rand_base(private_key.into(), &mut csprng)
        });
        let my_spendable_txos =
            generate_spendable_utxos(my_identity_tag, self.input_values, &mut csprng);
        let decoy_txos = generate_fake_input_txos(4, my_spendable_txos.len(), &mut csprng);

        let outputs = generate_outputs(private_key.into(), self.output_values, &mut csprng);
        let private_inputs = PrivateInputs {
            spends: my_spendable_txos,
            outputs,
            identity_input: my_identity_tag,
        };

        let banned_members = self.banned_members.unwrap_or_default();

        construct_send_claims_transaction(
            private_inputs,
            decoy_txos,
            private_key,
            banned_members,
            &mut csprng,
            |_, _| Ok(TEST_METADATA_BYTES.to_vec().into()),
        )
    }

    pub fn build<R>(self, csprng: R) -> SendClaimsTransaction
    where
        R: RngCore + CryptoRng,
    {
        self.build_result(csprng).unwrap()
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl Default for TestSendClaimsBuilder {
    fn default() -> Self {
        Self {
            input_values: vec![1, 2, 3, 4, 5],
            output_values: vec![5, 4, 3, 2, 1],
            id_source: None,
            private_key: None,
            banned_members: None,
        }
    }
}

/// Given a private key and some amounts, generate outputs for a transaction that correspond
/// to the amounts and are spendable by the key.
#[cfg(any(test, feature = "test-helpers"))]
pub fn generate_outputs<R: RngCore + CryptoRng>(
    owner: PublicKey,
    output_amounts: Vec<u64>,
    csprng: &mut R,
) -> Vec<SpendingOutput> {
    let mut public_keys: Vec<_> = output_amounts
        .iter()
        .map(|_| RistrettoPoint::random(csprng).into())
        .collect();

    let encryption_keys: Vec<_> = output_amounts
        .iter()
        .map(|_| EncryptionKey::random(csprng))
        .collect();

    // The first output is set to a one-time key associated with the private key of the issuer.
    // This is to simulate "change" output.
    // All of these are fake outputs for testing. The real ones are different.
    if let Some(key) = public_keys.get_mut(0) {
        *key = owner;
    }

    // Make the output amounts "owned" by the pubkeys we just imagined
    output_amounts
        .iter()
        .zip(public_keys)
        .zip(encryption_keys)
        .map(|((value, key), encryption_key)| SpendingOutput {
            value: *value,
            key,
            encryption_key,
        })
        .collect()
}

// TODO - add test coverage after breaking the cryptographic verification
//        out into smaller helper fns.
#[cfg(test)]
pub(crate) mod tests {
    use super::*;
    use crate::{
        crypto::DefaultCryptoImpl,
        test::{arb_rng, banned_members, insert_index},
    };
    use proptest::prelude::*;
    use rand::rngs::OsRng;

    #[test]
    fn test_transaction() {
        let csprng: OsRng = OsRng::default();
        let transaction = TestSendClaimsBuilder::default().build(csprng);
        assert!(
            verify_send_claims_transaction::<DefaultCryptoImpl>(&transaction, Vec::new()).is_ok()
        );
    }

    #[test]
    fn test_simplified_send_claims_construction() {
        // We will have 5 sets of input TXOs, one of which is the set of "real" inputs
        const NUM_INPUT_SETS: usize = 5;
        const NUM_INPUT_UTXOS: usize = 5;
        let mut csprng: OsRng = OsRng::default();

        // Things we know, going into the transaction:
        // * Our own identity (our public and private key)
        // * The amount we want to send
        // * Who we want to send it to (their public key)
        // * UTXOs that we own and can spend

        let our_private_key = PrivateKey::random(&mut csprng);

        //It would be selected from the input TxOs available in the chain. But for testing, we
        //Just create some.
        let amount_to_send: u64 = 5_000;
        let my_identity_tag =
            IdentityTag::from_key_with_rand_base(our_private_key.into(), &mut csprng);

        // Generate the "real" input UTXOs - the ones we own and can spend
        let real_input_amounts = vec![1_000_u64, 1_000, 1_000, 1_000, 1_000];
        let real_inputs =
            generate_spendable_utxos(my_identity_tag, real_input_amounts.clone(), &mut csprng);

        let output_amounts = vec![500_u64, 500, 2_000, 1_000, 1_000];

        assert_eq!(
            real_input_amounts.iter().sum::<u64>(),
            output_amounts.iter().sum::<u64>()
        );
        // And hence both are equal to the amount we want to send
        assert_eq!(amount_to_send, real_input_amounts.iter().sum::<u64>());
        // Make up some output public keys
        let outputs = generate_outputs(our_private_key.into(), output_amounts, &mut csprng);

        let private_inputs = PrivateInputs {
            spends: real_inputs,
            outputs,
            identity_input: my_identity_tag,
        };

        // Now we make up some things we don't know to simulate other selected TXOs that we would
        // use to mask our real UTXO.
        let fake_input_sets =
            generate_fake_input_txos(NUM_INPUT_SETS, NUM_INPUT_UTXOS, &mut csprng);

        let txn = construct_send_claims_transaction(
            private_inputs,
            fake_input_sets,
            our_private_key,
            Vec::new(),
            &mut csprng,
            |_, _| Ok(TEST_METADATA_BYTES.to_vec().into()),
        )
        .unwrap();
        assert!(verify_send_claims_transaction::<DefaultCryptoImpl>(&txn, Vec::new()).is_ok());
        assert!(!txn.core_transaction.output.is_empty());
        assert!(txn
            .core_transaction
            .output
            .iter()
            .all(|o| o.encrypted_metadata == TEST_METADATA_BYTES.to_vec().into()));
    }

    #[test]
    fn send_claims_encrypted_sender_uses_txo_output_keys() {
        // TODO: this will be fragile as it is dependent on verification order in the primary
        //       send claims verification function. This should be refactored to use a helper fn which
        //       explicitly verifies just the encryption portion of the send claims transaction.
        let mut rng = OsRng::default();
        let mut send_claims_transaction = TestSendClaimsBuilder::default().build(rng);
        // change the output txos to not match the encryption proof
        for output in send_claims_transaction.core_transaction.output.iter_mut() {
            output.txo = TransactionOutput::new(
                Default::default(),
                OneTimeKey::random(&mut rng),
                ESigPayload::new_with_ueta(),
            );
        }

        let result = verify_send_claims_transaction::<DefaultCryptoImpl>(
            &send_claims_transaction,
            Vec::new(),
        );
        assert!(matches!(
            result,
            Err(TransactionValidationError::InvalidEncryption(_))
        ))
    }

    #[test]
    fn will_not_construct_send_claims_transaction_with_inputs_bigger_than_outputs() {
        let rng = OsRng::default();
        let inputs = vec![4, 5, 6];
        let outputs = vec![1];
        let res = TestSendClaimsBuilder::default()
            .with_inputs(inputs)
            .with_outputs(outputs)
            .build_result(rng);
        assert!(matches!(
            res,
            Err(TransactionBuilderErrors::InputsDoNotMatchOutputValues)
        ))
    }

    #[test]
    fn will_not_construct_send_claims_transaction_with_outputs_bigger_than_inputs() {
        let rng = OsRng::default();
        let inputs = vec![1];
        let outputs = vec![4, 5, 6];
        let res = TestSendClaimsBuilder::default()
            .with_inputs(inputs)
            .with_outputs(outputs)
            .build_result(rng);
        assert!(matches!(
            res,
            Err(TransactionBuilderErrors::InputsDoNotMatchOutputValues)
        ))
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn can_send_claims_as_non_banned_member(
            banned_members in banned_members(0..10),
            rng in arb_rng()
        ) {
            let transaction = TestSendClaimsBuilder::default()
            .with_banned_members(banned_members.clone())
            .build(rng);
            assert!(verify_send_claims_transaction::<DefaultCryptoImpl>(&transaction, banned_members).is_ok());
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn cannot_verify_send_claims_with_wrong_banned_member(
            mut banned_members in banned_members(1..10),
            mut rng in arb_rng()
        ) {
            let transaction = TestSendClaimsBuilder::default()
            .with_banned_members(banned_members.clone())
            .build(&mut rng);

            // Replace one banned member
            let some_private_key = random_private_key(&mut rng);
            let some_public_key = (some_private_key * get_G()).into();
            let replace_index = insert_index(rng, banned_members.len());
            banned_members[replace_index] = some_public_key;

            assert!(matches!(
                verify_send_claims_transaction::<DefaultCryptoImpl>(&transaction, banned_members),
                Err(TransactionValidationError::InvalidAlpha)
            ));
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn banned_member_list_cannot_be_different_length_than_Qs(
            mut banned_members in banned_members(0..10),
            mut rng in arb_rng()
        ) {
            let transaction = TestSendClaimsBuilder::default()
            .with_banned_members(banned_members.clone())
            .build(&mut rng);

            // Add one banned member
            let some_private_key = random_private_key(&mut rng);
            let some_public_key = (some_private_key * get_G()).into();
            let replace_index = insert_index(rng, banned_members.len());
            banned_members.push(some_public_key);

            assert!(matches!(
                verify_send_claims_transaction::<DefaultCryptoImpl>(&transaction, banned_members),
                Err(TransactionValidationError::BannedMemberListDifferentLengths)
            ));
        }
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(2))]

        #[test]
        fn cannot_send_claims_as_banned_member(
            mut banned_members in banned_members(0..10),
            mut rng in arb_rng()
        ) {
            let own_private_key = random_private_key(&mut rng);
            let own_public_key = (own_private_key * get_G()).into();
            let insert_index = insert_index(&mut rng, banned_members.len());
            banned_members.insert(insert_index, own_public_key);

            let transaction = TestSendClaimsBuilder::default()
                .with_private_key(own_private_key)
                .with_banned_members(banned_members.clone())
                .build(rng);
            assert!(matches!(
                verify_send_claims_transaction::<DefaultCryptoImpl>(&transaction, banned_members),
                Err(TransactionValidationError::ConstructedByBannedMember)
            ));
        }
    }
}
