use crate::{
    transactions::PublicInputSet, CompactEncryptedSigner, CorrelationId, EncryptedBlob,
    IdentityTag, KeyImage, OpenedTransactionOutput, PrivateKey, Proof, PublicKey,
    RedeemCancellationReason, RistrettoPoint, SendClaimsOutput, TransactionResult,
    VerifiableEncryptionOfSignerKey,
};

use alloc::vec::Vec;
use serde::{Deserialize, Serialize};
use zkplmt::bulletproofs::BulletRangeProof;

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, Clone)]
pub enum RedeemSigner {
    EncryptedSigner(CompactEncryptedSigner),
    PublicKey(PublicKey),
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, Clone)]
pub struct ConfidentialRedeemRequestRecord {
    //Txo that is being redeemed. In the case of cancellation this will become spendable
    pub redeem_output: OpenedTransactionOutput,
    /// Encrypted data for the trust concerning where to send the cash transfer
    pub account_data: EncryptedBlob,
    /// The total amount to be redeemed
    pub amount: u64,
    /// Signer identity
    pub signer: RedeemSigner,
}

/// A record of a confidential unfulfilled redeem request a user does not submit this directly, rather it
/// is created and stored as a result of processing a redeem request transaction.

impl From<RedeemRequestTransaction> for ConfidentialRedeemRequestRecord {
    fn from(req: RedeemRequestTransaction) -> Self {
        ConfidentialRedeemRequestRecord {
            amount: req.core_transaction.output.redeem_output.value,
            redeem_output: req.core_transaction.output.redeem_output,
            account_data: req.core_transaction.extra,
            signer: RedeemSigner::EncryptedSigner(req.core_transaction.encrypted_sender.into()),
        }
    }
}

/// Issued by the trust to confirm a redeem fulfillment
#[derive(Deserialize, Serialize, Debug, Clone, derive_more::Constructor, PartialEq, Eq)]
pub struct RedeemFulfillmentTransaction {
    pub correlation_id: CorrelationId,
}

/// Issued by the trust (or intrinsically) to cancel an outstanding redeem request
#[derive(Deserialize, Serialize, Debug, Clone, derive_more::Constructor, PartialEq, Eq)]
pub struct RedeemCancellationTransaction {
    pub correlation_id: CorrelationId,
    pub reason: RedeemCancellationReason,
}

/// Encapsulates all the details of a [`RedeemRequestTransaction`] other than the proof with signature.
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[allow(non_snake_case)]
pub struct CoreRedeemRequestTransaction {
    /// A list of lists of transaction outputs where only one of the lists of outputs is the real
    // one being used.
    pub input: Vec<PublicInputSet>,

    /// Change output and redeem commitment
    pub output: RedeemOutput,

    /// The new identity tag produced when this transaction is processed, that can be used in
    /// subsequent transactions
    pub output_identity: IdentityTag,

    /// The correlation id matching this request and its fulfillment
    pub correlation_id: CorrelationId,

    /// key-images are nullifiers or the TransactionOutput(s). There is only a unique key-image for
    /// every TransactionOutput, but the key-image cannot be efficiently matched with the
    /// corresponding TransactionOutput by any polynomial time probabilistic algorithm with a
    /// non-negligible probability without using the private key. If a Transaction contains a
    /// key-image that has already been used, then the corresponding TransactionOutput has been
    /// spent already, which means that the transaction is invalid.
    pub key_images: Vec<KeyImage>,

    /// Z is used to store the randomness required to obfuscate which of the input sums equals the
    /// output sum. Without this randomness, all anonymity will be lost.
    pub Z: RistrettoPoint,

    /// A proof that Z is a Pedersen commitment to zero. It also implies that pZ is also a
    /// commitment to zero where p is any scalar.
    pub alpha: Proof,

    /// Proof that all the output TransactionOutput(s) contain commitments to values represented by
    /// a maximum number of bits. This is to stop the sum from rolling over to create money out of
    /// nothing.
    pub range_proof: BulletRangeProof,

    /// Banned members with randomness. Order must be kept to rebuild the alpha proof.
    pub Qs: Vec<RistrettoPoint>,

    ///The encrypted permanent public key of the sender or signer
    pub encrypted_sender: VerifiableEncryptionOfSignerKey,

    /// Private information
    pub extra: Vec<u8>,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct RedeemOutput {
    pub redeem_output: OpenedTransactionOutput,
    pub change_output: SendClaimsOutput,
}

/// A redeem request transaction
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct RedeemRequestTransaction {
    /// The underlying redeem request transaction which is signed for (part of) the proof
    pub core_transaction: CoreRedeemRequestTransaction,
    /// Works as both a signature and the proof that the signer is a verified participant.
    pub pi: Proof,
}

/// Inputs for a participant issuing a redeem request
#[derive(Debug, Deserialize, Serialize)]
pub struct PrivateRedeemRequestInputs {
    pub identity_input: IdentityTag,
    pub spends: Vec<OpenedTransactionOutput>,
    pub private_key: PrivateKey,
}

/// Implementors are capable of processing redeem request transactions
pub trait RedeemRequestHandler {
    /// Process and validate an incoming redeem request
    fn redeem_request(&self, transaction: RedeemRequestTransaction) -> TransactionResult;
}

/// Implementors are capable of processing redeem fulfillment transactions
pub trait RedeemFulfillmentHandler {
    /// Issued by the trust to indicate that it has transferred cash and fulfilled a redeem request
    fn redeem_fulfillment(&self, transaction: RedeemFulfillmentTransaction) -> TransactionResult;
}

/// Implementors are capable of processing redeem cancellation transactions
pub trait RedeemCancellationHandler {
    /// Issued to cancel an outstanding redeem request
    fn redeem_cancellation(&self, transaction: RedeemCancellationTransaction) -> TransactionResult;
}
