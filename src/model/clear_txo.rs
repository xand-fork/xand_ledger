use crate::{esig_payload::ESigPayload, model::monetary_value::MonetaryValue, PublicKey, Salt};
use core::fmt::Debug;
use curve25519_dalek::ristretto::RistrettoPoint;
use sha2::{Digest, Sha256};

pub trait IUtxo: Clone + Copy {
    type PublicKey: Eq;
    type Salt;

    fn esig_payload(&self) -> ESigPayload;
    fn value(&self) -> MonetaryValue;
    fn public_key(&self) -> Self::PublicKey;
    fn salt(&self) -> Self::Salt;
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct ClearTransactionOutput {
    public_key: PublicKey,
    value: MonetaryValue,
    // Reference write-up on salts and how they prevent replay attacks: https://www.parity.io/utxo-on-substrate/
    // See section starting with "Without the salt..."
    salt: Salt,
    esig_payload: ESigPayload,
}

impl IUtxo for ClearTransactionOutput {
    type PublicKey = PublicKey;
    type Salt = Salt;

    fn esig_payload(&self) -> ESigPayload {
        self.esig_payload
    }

    fn value(&self) -> MonetaryValue {
        self.value
    }

    fn public_key(&self) -> Self::PublicKey {
        self.public_key
    }

    fn salt(&self) -> Self::Salt {
        self.salt
    }
}

impl ClearTransactionOutput {
    pub fn new(
        public_key: PublicKey,
        value: MonetaryValue,
        salt: Salt,
        esig_payload: ESigPayload,
    ) -> Self {
        Self {
            public_key,
            value,
            salt,
            esig_payload,
        }
    }

    pub fn to_hash(&self) -> [u8; 32] {
        let mut hash = [0u8; 32];
        let pub_key_bytes = RistrettoPoint::from(self.public_key())
            .compress()
            .to_bytes();
        let salt_bytes = self.salt().to_be_bytes();
        let value_bytes = self.value().value().to_be_bytes();

        let mut hasher = Sha256::new();
        hasher.input(&pub_key_bytes[0..32]);
        hasher.input(salt_bytes);
        hasher.input(value_bytes);

        hash.copy_from_slice(hasher.result().as_slice());
        hash
    }
}

#[cfg(test)]
pub(crate) mod tests {
    #![allow(non_snake_case)]

    use super::*;
    use std::convert::TryFrom;
    use std::hash::{Hash, Hasher};

    impl ClearTransactionOutput {
        /// Creates an `ClearTransactionOutput` with a valid paylod.
        /// Note: We currently only implement e-signatures for UETA networks.
        /// For custom payloads, use `with_custom_esig_payload()` instead.
        pub fn with_compliant_esig_payload(
            public_key: PublicKey,
            value: MonetaryValue,
            salt: Salt,
        ) -> Self {
            Self {
                public_key,
                value,
                salt,
                esig_payload: ESigPayload::new_with_ueta(),
            }
        }

        /// Creates an `ClearTransactionOutput` with a custom payload.
        /// Note: Any payload that is not a valid UETA payload will not be accepted by the network
        /// as we currently only implement e-signatures for UETA networks.
        /// For valid payloads, use `with_compliant_esig_payload()` instead.
        pub fn with_custom_esig_payload(
            public_key: PublicKey,
            value: MonetaryValue,
            salt: Salt,
            esig_text: &'static str,
        ) -> Result<Self, crate::esig_payload::errors::ESigError> {
            Ok(Self {
                public_key,
                value,
                salt,
                esig_payload: ESigPayload::new(esig_text)?,
            })
        }
    }

    #[allow(clippy::derive_hash_xor_eq)]
    impl Hash for ClearTransactionOutput {
        fn hash<H: Hasher>(&self, state: &mut H) {
            self.public_key.hash(state);
            self.salt.hash(state);
            self.value.hash(state);
        }
    }

    #[test]
    fn to_hash__produces_same_hash_for_outputs_with_same_contents() {
        // Given
        let public_key = PublicKey::from("key");
        let value = MonetaryValue::try_from(100u64).unwrap();
        let salt = 1;
        let esig_payload = ESigPayload::new_with_ueta();
        let clear_txo_1 = ClearTransactionOutput::new(public_key, value, salt, esig_payload);
        let clear_txo_2 = ClearTransactionOutput::new(public_key, value, salt, esig_payload);

        // When
        let hash_1 = clear_txo_1.to_hash();
        let hash_2 = clear_txo_2.to_hash();

        // Then
        assert_eq!(hash_1, hash_2)
    }

    #[test]
    fn to_hash__produces_different_hash_for_outputs_that_differ_in_only_owner() {
        // Given
        let public_key_1 = PublicKey::from("key");
        let public_key_2 = PublicKey::from("other_key");
        let value = MonetaryValue::try_from(100u64).unwrap();
        let salt = 1;
        let esig_payload = ESigPayload::new_with_ueta();
        let clear_txo_1 = ClearTransactionOutput::new(public_key_1, value, salt, esig_payload);
        let clear_txo_2 = ClearTransactionOutput::new(public_key_2, value, salt, esig_payload);

        // When
        let hash_1 = clear_txo_1.to_hash();
        let hash_2 = clear_txo_2.to_hash();

        // Then
        assert_ne!(hash_1, hash_2)
    }

    #[test]
    fn to_hash__produces_different_hash_for_outputs_that_differ_in_only_value() {
        // Given
        let public_key = PublicKey::from("key");
        let value_1 = MonetaryValue::try_from(100u64).unwrap();
        let value_2 = MonetaryValue::try_from(200u64).unwrap();
        let salt = 1;
        let esig_payload = ESigPayload::new_with_ueta();
        let clear_txo_1 = ClearTransactionOutput::new(public_key, value_1, salt, esig_payload);
        let clear_txo_2 = ClearTransactionOutput::new(public_key, value_2, salt, esig_payload);

        // When
        let hash_1 = clear_txo_1.to_hash();
        let hash_2 = clear_txo_2.to_hash();

        // Then
        assert_ne!(hash_1, hash_2)
    }

    #[test]
    fn to_hash__produces_different_hash_for_outputs_that_differ_in_only_salt() {
        // Given
        let public_key = PublicKey::from("key");
        let value = MonetaryValue::try_from(100u64).unwrap();
        let salt_1 = 1;
        let salt_2 = 2;
        let esig_payload = ESigPayload::new_with_ueta();
        let clear_txo_1 = ClearTransactionOutput::new(public_key, value, salt_1, esig_payload);
        let clear_txo_2 = ClearTransactionOutput::new(public_key, value, salt_2, esig_payload);

        // When
        let hash_1 = clear_txo_1.to_hash();
        let hash_2 = clear_txo_2.to_hash();

        // Then
        assert_ne!(hash_1, hash_2)
    }
}
