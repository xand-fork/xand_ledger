use alloc::{string::String, vec::Vec};
use core::{
    convert::TryFrom,
    fmt::{Display, Error, Formatter},
    str::FromStr,
};
#[cfg(any(test, feature = "test-helpers"))]
use rand::{CryptoRng, Rng};
use serde::{Deserialize, Serialize};

/// A Encryption key representing the public portion of some private key, and hence a (potential)
/// sender or receiver of encrypted messages.
///
/// Currently, this means a Base58 encoded string
#[derive(Clone, Copy, Debug, Deserialize, Hash, Eq, PartialEq, Serialize)]
pub struct EncryptionKey([u8; 32]);

impl EncryptionKey {
    pub fn as_bytes(&self) -> &[u8; 32] {
        &self.0
    }

    #[cfg(any(test, feature = "test-helpers"))]
    pub fn random<R: Rng + CryptoRng>(csprng: &mut R) -> Self {
        EncryptionKey(csprng.gen())
    }
}

impl Display for EncryptionKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        let encoded_key = bs58::encode(self.0).into_string();
        Display::fmt(&encoded_key, f)
    }
}

impl TryFrom<String> for EncryptionKey {
    type Error = EncryptionKeyError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        value.parse()
    }
}

impl FromStr for EncryptionKey {
    type Err = EncryptionKeyError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let key = bs58::decode(s)
            .into_vec()
            .map_err(|e| EncryptionKeyError::Base58Encode { source: e })?;
        EncryptionKey::try_from(key.as_slice())
    }
}

impl TryFrom<&[u8]> for EncryptionKey {
    type Error = EncryptionKeyError;

    fn try_from(slice: &[u8]) -> Result<Self, Self::Error> {
        Ok(<[u8; 32]>::try_from(slice)
            .map_err(|_| EncryptionKeyError::InvalidKeyLength)?
            .into())
    }
}

impl From<[u8; 32]> for EncryptionKey {
    fn from(bytes: [u8; 32]) -> Self {
        EncryptionKey(bytes)
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum EncryptionKeyError {
    // Encryption key wasn't proper base 58
    Base58Encode { source: bs58::decode::Error },
    // Invalid encryption key length, expected 32 bytes
    InvalidKeyLength,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize, Default)]
pub struct Encrypted(Vec<u8>);

impl Encrypted {
    pub fn as_slice(&self) -> &[u8] {
        self.0.as_slice()
    }
}

impl From<Vec<u8>> for Encrypted {
    fn from(bytes: Vec<u8>) -> Self {
        Encrypted(bytes)
    }
}

impl From<Encrypted> for Vec<u8> {
    fn from(enc: Encrypted) -> Self {
        enc.0
    }
}
