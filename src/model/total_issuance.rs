/// Trait for tracking the current total number of claims
pub trait TotalIssuanceRepository {
    /// Increase the total cash confirmed claims by the provided amount
    fn add_to_total_cash_confirmations(&self, amount: u64) -> u64;
    /// Get the total number of claims that have *ever* been cash confirmed on the network. Note that
    /// this is **not** the total number of claims *currently present* on the network.
    fn get_total_cash_confirmations(&self) -> u64;

    /// Increase the total redeemed claims amount by the provided amount
    fn add_to_total_redeemed(&self, amount: u64) -> u64;

    /// Decrease the total redeemed claims amount by the provided amount
    fn subtract_from_total_redeemed(&self, amount: u64) -> u64;

    /// Get the total number of claims that have *ever* been redeemed on the network.
    fn get_total_redeemed(&self) -> u64;

    fn get_total_claims(&self) -> u64 {
        self.get_total_cash_confirmations() - self.get_total_redeemed()
    }
}
