use crate::{
    CoreCreateRequestTransaction, CoreExitingRedeemRequestTransaction,
    CoreRedeemRequestTransaction, CoreSendClaimsTransaction,
};
use alloc::vec::Vec;
use serde::Serialize;

/// This trait is a marker trait that can be applied to any struct implementing serde serialization.
/// In practice, in this code it should only be applied to the "core" parts of transactions which
/// need to be serialized so that they can be signed.
///
/// The trait is a marker trait in the sense that it provides a default implementation which uses
/// the parity SCALE codec for encoding. There is no reason to override this default, as all
/// transactions ought to be encoded consistently. It leaves us the option to swap out the
/// implementation for all of them at once.
pub(crate) trait SerializableForSigning: Serialize {
    /// Produce bytes from this transaction which are ready for signing
    fn signable_bytes(&self) -> Vec<u8> {
        serde_scale::to_vec(&self).expect("serde_scale::to_vec is infallible")
    }
}

impl SerializableForSigning for CoreCreateRequestTransaction {}
impl SerializableForSigning for CoreSendClaimsTransaction {}
impl SerializableForSigning for CoreRedeemRequestTransaction {}
impl SerializableForSigning for CoreExitingRedeemRequestTransaction {}
