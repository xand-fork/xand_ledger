use crate::model::monetary_value::errors::MonetaryValueError;
use core::{
    convert::{TryFrom, TryInto},
    num::NonZeroU64,
};
use serde::{Deserialize, Serialize};

#[derive(Copy, Clone, Hash, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct MonetaryValue(NonZeroU64);

impl MonetaryValue {
    pub fn value(&self) -> u64 {
        self.0.get()
    }
}

impl TryFrom<u32> for MonetaryValue {
    type Error = MonetaryValueError;

    fn try_from(val: u32) -> Result<Self, Self::Error> {
        let val: u64 = val.into();
        val.try_into()
    }
}

impl TryFrom<u64> for MonetaryValue {
    type Error = MonetaryValueError;

    fn try_from(val: u64) -> Result<Self, Self::Error> {
        val.try_into().map(Self).map_err(Into::into)
    }
}

pub mod errors {
    use core::num::TryFromIntError;
    use serde::{Deserialize, Serialize};

    #[derive(Debug, PartialEq, Eq, Clone, Deserialize, Serialize)]
    pub enum MonetaryValueError {
        TryFromIntError,
    }

    impl From<TryFromIntError> for MonetaryValueError {
        fn from(err: TryFromIntError) -> Self {
            MonetaryValueError::TryFromIntError
        }
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn try_from__u64_non_zero_amount_is_ok() {
        // Given
        let amt = 1_u64;

        // When
        let res = MonetaryValue::try_from(amt);

        // Then
        assert!(res.is_ok(), "Actual: {:?}", res);
    }

    #[test]
    fn try_from__u64_zero_amount_is_err() {
        // Given
        let amt = 0_u64;

        // When
        let res = MonetaryValue::try_from(amt);

        // Then
        assert!(
            matches!(res, Err(MonetaryValueError::TryFromIntError)),
            "Actual: {:?}",
            res
        );
    }

    #[test]
    fn try_from__u32_non_zero_amt_is_ok() {
        // Given
        let amt = 1_u32;

        // When
        let res = MonetaryValue::try_from(amt);

        // Then
        assert!(res.is_ok(), "Actual: {:?}", res);
    }

    #[test]
    fn try_from__u32_zero_amount_is_err() {
        // Given
        let amt = 0_u32;

        // When
        let res = MonetaryValue::try_from(amt);

        // Then
        assert!(
            matches!(res, Err(MonetaryValueError::TryFromIntError)),
            "Actual: {:?}",
            res
        );
    }
}
