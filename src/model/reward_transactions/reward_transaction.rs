use crate::clear_txo::IUtxo;

#[derive(Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct RewardTransaction<Utxo: IUtxo> {
    pub(crate) utxo: Utxo,
}

impl<Utxo: IUtxo> RewardTransaction<Utxo> {
    pub fn new(txo: Utxo) -> Self {
        Self { utxo: txo }
    }

    pub fn utxo(&self) -> Utxo {
        self.utxo
    }
}
