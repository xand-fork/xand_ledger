use crate::{
    clear_txo::IUtxo, model::reward_transactions::reward_transaction::RewardTransaction,
    TransactionResult,
};

/// Implementors are capable of processing reward transactions
pub trait RewardTransactionHandler<Utxo: IUtxo> {
    /// Process an incoming reward transaction
    fn process_reward_transaction(
        &self,
        reward_transaction: RewardTransaction<Utxo>,
    ) -> TransactionResult;
}
