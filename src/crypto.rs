use crate::{
    transactions::txn_input_keys_to_hash_curve_points, verify_zkplmt, TransactionOutput,
    VectorTuple, ZkPlmtError,
};
use alloc::vec::Vec;
use curve25519_dalek::ristretto::RistrettoPoint;
pub use zkplmt::bulletproofs::BulletRangeProof;
use zkplmt::{
    bulletproofs::{bullet_range_verify, Bases},
    core::CryptoSystemParameters,
    models::Proof,
};

lazy_static::lazy_static! {
    static ref CRYPTO_SYSTEM: CryptoSystemParameters = serde_scale::from_slice(include_bytes!(concat!(env!("OUT_DIR"), "/crypto_params"))).unwrap();
}

#[allow(non_snake_case)]
pub(crate) fn get_K() -> RistrettoPoint {
    CRYPTO_SYSTEM.K
}

#[allow(non_snake_case)]
pub(crate) fn get_L() -> RistrettoPoint {
    CRYPTO_SYSTEM.L
}

#[allow(non_snake_case)]
pub(crate) fn get_G() -> RistrettoPoint {
    CRYPTO_SYSTEM.G
}

pub(crate) fn get_bases() -> &'static Bases {
    &CRYPTO_SYSTEM.bases
}

/// For computationally heavy operations that may need different implementations
/// in various contexts (e.g. WASM vs Native)
pub trait LedgerCrypto {
    fn verify_bulletproof(proof: &BulletRangeProof) -> bool;

    fn verify_zkplmt(
        message: &[u8],
        tuples: &[VectorTuple],
        proof: &Proof,
    ) -> Result<(), ZkPlmtError>;

    fn txn_input_keys_to_hash_curve_points(input: &[TransactionOutput]) -> Vec<RistrettoPoint>;
}

/// Reference implementation of LedgerCrypto
pub struct DefaultCryptoImpl;

impl LedgerCrypto for DefaultCryptoImpl {
    fn verify_bulletproof(proof: &BulletRangeProof) -> bool {
        bullet_range_verify(proof, get_bases())
    }

    fn verify_zkplmt(
        message: &[u8],
        tuples: &[VectorTuple],
        proof: &Proof,
    ) -> Result<(), ZkPlmtError> {
        verify_zkplmt(message, tuples, proof)
    }

    fn txn_input_keys_to_hash_curve_points(input: &[TransactionOutput]) -> Vec<RistrettoPoint> {
        txn_input_keys_to_hash_curve_points(input)
    }
}
