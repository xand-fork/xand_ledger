//! This module contains reward validation logic, as well as helpers for processing
//! rewards

#[cfg(test)]
mod tests;

use crate::{
    alloc::string::ToString,
    esig_payload::{ESigPayload, UETA_TEXT},
    model::clear_txo::IUtxo,
    reward_transactions::RewardTransaction,
    ClearTransactionOutputRepository, FailureReason, RewardTransactionHandler,
    TotalIssuanceRepository, TransactionResult,
};

pub struct RewardProcessor<'a, Store: ClearTransactionOutputRepository> {
    utxo_repo: &'a Store,
    total_issuance_repo: &'a dyn TotalIssuanceRepository,
}

impl<'a, Store: ClearTransactionOutputRepository> RewardProcessor<'a, Store> {
    pub fn new(utxo_repo: &'a Store, total_issuance_repo: &'a dyn TotalIssuanceRepository) -> Self {
        Self {
            utxo_repo,
            total_issuance_repo,
        }
    }

    fn verify_output_doesnt_exist(
        &self,
        txn: &RewardTransaction<Store::Utxo>,
    ) -> Result<(), FailureReason> {
        if self.utxo_repo.contains(&txn.utxo) {
            return Err(FailureReason::OutputTxoAlreadyExists);
        }
        Ok(())
    }

    fn verify_output_is_ueta_compliant(
        &self,
        txn: &RewardTransaction<Store::Utxo>,
    ) -> Result<(), FailureReason> {
        if !self.reward_transaction_output_contains_valid_ueta(txn) {
            Err(FailureReason::TxoNotUETACompliant)
        } else {
            Ok(())
        }
    }

    fn reward_transaction_output_contains_valid_ueta(
        &self,
        txn: &RewardTransaction<Store::Utxo>,
    ) -> bool {
        matches!(txn.utxo.esig_payload(), ESigPayload::Ueta(val) if val.to_string() == UETA_TEXT)
    }
}

impl<'a, Store: ClearTransactionOutputRepository> RewardTransactionHandler<Store::Utxo>
    for RewardProcessor<'a, Store>
{
    fn process_reward_transaction(
        &self,
        reward_transaction: RewardTransaction<Store::Utxo>,
    ) -> TransactionResult {
        self.verify_output_doesnt_exist(&reward_transaction)?;
        self.verify_output_is_ueta_compliant(&reward_transaction)?;

        let transaction_amount = reward_transaction.utxo.value().value();

        self.utxo_repo.add(reward_transaction.utxo);

        self.total_issuance_repo
            .add_to_total_cash_confirmations(transaction_amount);

        Ok(())
    }
}
