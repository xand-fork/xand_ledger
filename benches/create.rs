use criterion::{black_box, criterion_group, Criterion};
use rand::rngs::OsRng;
use xand_ledger::{
    create_banned_members_list, crypto::DefaultCryptoImpl, verify_create_request_transaction,
    TestCreateRequestBuilder,
};

criterion_group!(
    create,
    create_request_construction,
    create_request_construction_with_banned,
    create_request_verification,
    create_request_verification_with_banned
);

fn create_request_construction(c: &mut Criterion) {
    c.bench_function("create request construction", |b| {
        b.iter(|| TestCreateRequestBuilder::default().build(black_box(&mut OsRng::default())))
    });
}

fn create_request_construction_with_banned(c: &mut Criterion) {
    let banned_count = 5;
    let description = format!("create request construction with {} banned", banned_count);
    let rng = OsRng::default();
    let banned_members = create_banned_members_list(rng, banned_count);
    c.bench_function(&description, |b| {
        b.iter(|| {
            TestCreateRequestBuilder::default()
                .banned_members(banned_members.clone())
                .build(black_box(&mut OsRng::default()))
        })
    });
}

fn create_request_verification(c: &mut Criterion) {
    let transaction_response = TestCreateRequestBuilder::default().build(&mut OsRng::default());
    c.bench_function("create request verification", |b| {
        b.iter(|| {
            verify_create_request_transaction::<DefaultCryptoImpl>(
                black_box(&transaction_response.create_request),
                &transaction_response.trust_pub_key,
                Vec::new(),
            )
        })
    });
}

fn create_request_verification_with_banned(c: &mut Criterion) {
    let banned_count = 5;
    let description = format!("create request verification with {} banned", banned_count);
    let rng = OsRng::default();
    let banned_members = create_banned_members_list(rng, banned_count);
    let transaction_response = TestCreateRequestBuilder::default()
        .banned_members(banned_members.clone())
        .build(&mut OsRng::default());
    c.bench_function(&description, |b| {
        b.iter(|| {
            verify_create_request_transaction::<DefaultCryptoImpl>(
                black_box(&transaction_response.create_request),
                &transaction_response.trust_pub_key,
                banned_members.clone(),
            )
        })
    });
}
