mod create;
mod redeem;
mod send_claims;

use create::create;
use criterion::criterion_main;
use redeem::redeem;
use send_claims::send_claims;

criterion_main!(send_claims, create, redeem);
